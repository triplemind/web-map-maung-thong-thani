<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', function () {
//     return \Redirect::route('auth.login.get');
// });


Route::get('/test', 'Test\TestController@Index');

// Route::get('/dashboard', 'Dashboard\DashboardController@Index');

Route::get('/auth/login', [
	'uses'	=> 'Auth\LoginController@getLogin',
	'as'	=> 'auth.login.get'
]);

Route::post('/auth/login', [
	'uses'	=> 'Auth\LoginController@postLogin',
	'as'	=> 'auth.login.post'
]);

Route::get('/auth/register', [
	'uses'	=> 'Auth\RegisterController@getRegister',
	'as'	=> 'auth.register.get'
]);

Route::post('/auth/register', [
	'uses'	=> 'Auth\RegisterController@postRegister',
	'as'	=> 'auth.register.post'
]);

Route::get('/auth/logout', [
	'uses'	=> 'Auth\LoginController@logout',
	'as'	=> 'auth.logout.get'
]);


Route::get('/', [
	'uses'	=> 'Dashboard\DashboardController@Index',
	'as'	=> 'dash.index.get'
]);

Route::post('/ajax_center', [
  'uses'  => 'Dashboard\DashboardController@ajaxCenter',
  'as'  => 'dash.ajax_center.post'
]);

Route::post('/postaddshare', [
  'uses'  => 'Dashboard\DashboardController@postAddShare',
  'as'  => 'dash.postaddshare.post'
]);

Route::post('/postaddRate', [
  'uses'  => 'Dashboard\DashboardController@postAddRate',
  'as'  => 'dash.postaddRate.post'
]);


// USER
Route::get('/user', [
	'uses'	=> 'User\UserController@Index',
	'as'	=> 'user.index.get'
]);

Route::post('/user/ajax_center', [
    'uses'  => 'User\UserController@ajaxCenter',
    'as'  => 'user.ajax_center.post'
  ]);

  Route::post('/user/add', [
    'uses'  => 'User\UserController@postAdd',
    'as'  => 'user.add.post'
  ]);

 Route::post('/user/edit', [
    'uses'  => 'User\UserController@postEdit',
    'as'  => 'user.edit.post'
  ]);

Route::post('/user/delete', [
    'uses'  => 'User\UserController@postRemove',
    'as'  => 'user.delete.post'
  ]);



// profile
Route::get('/profile', [
    'uses'  => 'Profile\ProfileController@Index',
    'as'  => 'profile.index.get'
  ]);

Route::post('/profile/edit', [
    'uses'  => 'Profile\ProfileController@postEdit',
    'as'  => 'profile.edit.post'
  ]);



//PLACE
Route::get('/place', [
    'uses'  => 'Place\PlaceController@Index',
    'as'  => 'place.index.get'
]);

Route::get('/place/add', [
    'uses'  => 'Place\PlaceController@getAdd',
    'as'  => 'place.add.get'
]);

Route::post('/place/add', [
    'uses'  => 'Place\PlaceController@postAdd',
    'as'  => 'place.add.post'
]);

Route::get('/place/edit/{id}', [
    'uses'  => 'Place\PlaceController@getEdit',
    'as'  => 'place.edit.get'
]);

Route::post('/place/edit', [
    'uses'  => 'Place\PlaceController@postEdit',
    'as'  => 'place.edit.post'
]);

Route::post('/place/ajax_center', [
    'uses'  => 'Place\PlaceController@ajaxCenter',
    'as'  => 'place.ajax_center.post'
]);

Route::post('/place/delete', [
    'uses'  => 'Place\PlaceController@postRemove',
    'as'  => 'place.delete.post'
]);

//PLACE



//SERVICE
Route::get('/service', [
    'uses'  => 'ServiceData\ServiceDataController@Index',
    'as'  => 'service.index.get'
]);

Route::get('/service/add', [
    'uses'  => 'ServiceData\ServiceDataController@getAdd',
    'as'  => 'service.add.get'
]);

Route::post('/service/add', [
    'uses'  => 'ServiceData\ServiceDataController@postAdd',
    'as'  => 'service.add.post'
]);

Route::get('/service/edit/{id}', [
    'uses'  => 'ServiceData\ServiceDataController@getEdit',
    'as'  => 'service.edit.get'
]);

Route::post('/service/edit', [
    'uses'  => 'ServiceData\ServiceDataController@postEdit',
    'as'  => 'service.edit.post'
]);

Route::post('/service/ajax_center', [
    'uses'  => 'ServiceData\ServiceDataController@ajaxCenter',
    'as'  => 'service.ajax_center.post'
]);


Route::post('/service/delete', [
    'uses'  => 'ServiceData\ServiceDataController@postRemove',
    'as'  => 'service.delete.post'
]);

//SERVICE



//TRANSPORT
Route::get('/transport', [
    'uses'  => 'Transport\TransportController@Index',
    'as'  => 'transport.index.get'
]);

Route::get('/transport/add', [
    'uses'  => 'Transport\TransportController@getAdd',
    'as'  => 'transport.add.get'
]);

Route::post('/transport/add', [
    'uses'  => 'Transport\TransportController@postAdd',
    'as'  => 'transport.add.post'
]);

Route::get('/transport/edit/{id}', [
    'uses'  => 'Transport\TransportController@getEdit',
    'as'  => 'transport.edit.get'
]);

Route::post('/transport/edit', [
    'uses'  => 'Transport\TransportController@postEdit',
    'as'  => 'transport.edit.post'
]);

Route::post('/transport/ajax_center', [
    'uses'  => 'Transport\TransportController@ajaxCenter',
    'as'  => 'transport.ajax_center.post'
]);


Route::post('/transport/delete', [
    'uses'  => 'Transport\TransportController@postRemove',
    'as'  => 'transport.delete.post'
]);

//TRANSPORT

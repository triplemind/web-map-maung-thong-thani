<?php

namespace App\Http\Controllers\User;

// use App\Http\Requests;
use Illuminate\Http\Request;
use App\Services\Test\Test;
use App\Http\Controllers\Controller;
use App\Services\Users\UserObject;
use App\Services\Users\User;
use App\Http\Requests\User\UserFormRequest;
use App\Services\Users\UserRepository;

class UserController extends Controller
{
	public function Index()
	{
		$users = User::all();

		return $this->view('user.index', compact('users'));
	}

	public function postAdd(UserFormRequest $userRequest)
	{
		$username 	= \Input::has('username') ? \Input::get('username') : '';
		$password 	= \Input::has('password') ? \Input::get('password') : '';
		$email 		= \Input::has('email') ? \Input::get('email') : '';
		$user_type 	= \Input::has('user_type') ? \Input::get('user_type') : '';
		$first_name = \Input::has('first_name') ? \Input::get('first_name') : '';
		$last_name 	= \Input::has('last_name') ? \Input::get('last_name') : '';
		$image 		= \Input::hasFile('image') ? \Input::file('image') : '';

		$image_name = "";
		$date = date('Y-m-d');

		$path = base_path();
		if(!file_exists($path."/public/profile_img")){
			$oldmask = umask(0);
			mkdir($path."/public/profile_img", 0777);
			umask($oldmask);
		}

		if(!empty($image))
		{
			$image_name = "/public/profile_img/".$date."-".$image->getClientOriginalName();
			copy($image, $path.$image_name);
		}

		$new_user 				= new User;
		$new_user->first_name 	= $first_name;
		$new_user->last_name 	= $last_name;
		$new_user->username 	= $username;
		$new_user->password 	= $password;
		$new_user->user_img 	= (empty($image_name)) ? "" : $image_name;
		$new_user->email 		= $email;
		$new_user->user_type 	= $user_type;
		$new_user->save();
		

		return ['status' => 'success'];
	}


	public function postEdit()
	{
		$user_id 	= \Input::has('user_id') ? \Input::get('user_id') : '';
		$username 	= \Input::has('username') ? \Input::get('username') : '';
		$password 	= \Input::has('password') ? \Input::get('password') : '';
		$email 		= \Input::has('email') ? \Input::get('email') : '';
		$user_type 	= \Input::has('user_type') ? \Input::get('user_type') : '';
		$first_name = \Input::has('first_name') ? \Input::get('first_name') : '';
		$last_name 	= \Input::has('last_name') ? \Input::get('last_name') : '';
		$image 		= \Input::hasFile('image') ? \Input::file('image') : '';

		$chk_username	= User::where('username', $username)->count();
		if($chk_username > 1) return helperReturnErrorFormRequestArray(['username' => 'Username is ready exits.']);

		$chk_email	= User::where('email', $email)->count();
		if($chk_email > 1) return helperReturnErrorFormRequestArray(['email' => 'Email is ready exits.']);

		$user		= User::where('user_id', $user_id)->first();
		if(empty($user)) return helperReturnErrorFormRequest('Not found Data.');

		$image_name = "";
		$date = date('Y-m-d');

		$path = base_path();
		if(!file_exists($path."/public/profile_img")){
			$oldmask = umask(0);
			mkdir($path."/public/profile_img", 0777);
			umask($oldmask);
		}

		if(!empty($image))
		{
			if(!empty($user->image)){
				if(file_exists($path.$user->image)){
					unlink($path.$user->image);
				}
			}

			$image_name = "/public/profile_img/".$date."-".$image->getClientOriginalName();
			copy($image, $path.$image_name);
		}



		$user->first_name 	= $first_name;
		$user->last_name 	= $last_name;
		$user->username 	= $username;
		if ($password != '') $user->password 	= $password;
		if(!empty($image_name)){
			$user->user_img 	= (empty($image_name)) ? "" : $image_name;
		}
		$user->email 		= $email;
		$user->user_type 	= $user_type;
		$user->save();


		return ['status' => 'success'];
	}

	public function postRemove()
	{
		$user_id  	= \Input::has('user_id') ? \Input::get('user_id') : '';
		$user		= User::where('user_id', $user_id)->first();

		if(empty($user)) return helperReturnErrorFormRequest('Not found Data.');

		$user->delete();

		return ['status' => 'success'];
	}

	public function ajaxCenter()
	{
		$method  	= \Input::has('method') ? \Input::get('method') : '';
		$user_repo 	= new UserRepository;

		switch ($method) {
			case 'getFormAddUser':
				
				$form  = $user_repo->genFormAddUser();

				return ['status' => 'success', 'form' => $form];
				break;

			case 'getFormEditUser':
				
				$user_id 	= \Input::has('user_id') ? \Input::get('user_id') : '';

				if(empty($user_id)) return ['status' => 'error', 'msg' => 'field user_id is required.'];

				$user 	= User::where('user_id', $user_id)->first();
				if(empty($user)) return ['status' => 'error', 'msg' => 'field user_id is required.'];

				$form  	= $user_repo->genFormEditUser($user);

				return ['status' => 'success', 'form' => $form];
				break;

			default:
				return ['status' => 'error', 'msg' => 'Not found method'];
				break;
		}
	}


}
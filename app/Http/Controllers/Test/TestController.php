<?php

namespace App\Http\Controllers\Test;

// use App\Http\Requests;
use Illuminate\Http\Request;
use App\Services\Test\Test;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
	public function Index()
	{
		$tests = Test::all();

		return $this->view('Test.index', compact('tests'));
	}

}
<?php

namespace App\Http\Controllers\Auth;

// use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Auths\UserAuth;
use App\Services\Users\User;


class RegisterController extends Controller
{
    public function getRegister()
    {

        return view('auth.register');
    }

    public function postRegister()
    {

        $username   = \Input::has('username') ? \Input::get('username') : '';
        $password   = \Input::has('password') ? \Input::get('password') : '';
        $email      = \Input::has('email') ? \Input::get('email') : '';
        $first_name = \Input::has('first_name') ? \Input::get('first_name') : '';
        $last_name  = \Input::has('last_name') ? \Input::get('last_name') : '';
        
        // d($first_name);
        // d($last_name);
        // d($username);
        // d($password);
        // sd($email);

        if($username == "") return ['status' => 'error', 'msg' => 'Username cannot be null.'];
        if($password == "") return ['status' => 'error', 'msg' => 'Passsword cannot be null.'];
        if($email == "") return ['status' => 'error', 'msg' => 'Email cannot be null.'];
        if($first_name == "") return ['status' => 'error', 'msg' => 'First Name cannot be null.'];
        if($last_name == "") return ['status' => 'error', 'msg' => 'Last Name cannot be null.'];

        // ตรวจสอบว่ามี username นี้ในระบบแล้วหรือยัง
        $chk_username   = User::where('username', $username)->count();
        // ถ้ามีก็ return error กลับไป
        if($chk_username >= 1) return ['status' => 'error', 'msg' => 'Username is ready exits.'];

        // ตรวจสอบว่ามี username นี้ในระบบแล้วหรือยัง
        $chk_email  = User::where('email', $email)->count();
        // ถ้ามีก็ return error กลับไป
        if($chk_email >= 1) return ['status' => 'error', 'msg' => 'Email is ready exits.'];


        $new_register               = new User;
        $new_register->first_name   = $first_name;
        $new_register->last_name    = $last_name;
        $new_register->username     = $username;
        $new_register->password     = $password;
        $new_register->user_img     = empty($image_name) ? "" : $image_name;
        $new_register->email        = $email;
        $new_register->user_type    = 'user';
        $new_register->save();

        return ['status' => 'success'];

    }
}

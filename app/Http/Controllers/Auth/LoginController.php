<?php

namespace App\Http\Controllers\Auth;

use App;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Services\Auths\UserAuth;
use App\Services\Users\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getLogin()
    {
        if (UserAuth::check()) {
            return \Redirect::route('dash.index.get');
        }

        return view('auth.login');
    }

     public function postLogin()
    {
        

        $username = \Input::has('username') ? \Input::get('username') : '';
        $password = \Input::has('password') ? \Input::get('password') : '';

        if ($user = UserAuth::attempt($username, $password)) {

            return \Redirect::route('dash.index.get');

        }

        // Redirect to Login Page.
        return redirect("/auth/login")->withInput(\Input::except('password'))
                ->withErrors(['username' => 'Invalid Username or Password']);
        $user   = User::where('username', $username)->where('password', $password)->first();

        $login_page = 'auth.login';
        return view($login_page);
    }

        public function logout()
    {
        UserAuth::clear();

        return \Redirect::route('dash.index.get');
    }

}

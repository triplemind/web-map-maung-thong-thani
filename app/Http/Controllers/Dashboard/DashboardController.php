<?php

namespace App\Http\Controllers\Dashboard;

// use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Test\Test;
use App\Services\Users\User;
use App\Services\Place\Place;
use App\Services\PlaceType\PlaceType;
use App\Services\PlaceTypeDescription\PlaceTypeDescription;
use App\Services\Review\Review;
use App\Services\Share\Share;
use App\Services\Transport\Transport;
use App\Services\TransportType\TransportType;
use App\Services\TransportTypeDescription\TransportTypeDescription;

class DashboardController extends Controller
{
	public function Index()
	{
		$dash = "ระบบช่วยเหลือการเดินทางในเมืองทอง ***";
		$user_obj = \Session::has('current_user') ? \Session::get('current_user') : '';

		// sd($userObject);

		return $this->view('dashboard.index', compact('dash','user_obj'));
	}

	public function postAddShare(){

		$action_sh = \Input::has('action_sh') ? \Input::get('action_sh') : "";

		$date = date("l");

        $share = Share::where('day', date("d"));
        $share = $share->where('month', date("F"));
        $share = $share->where('year', date("Y"));
        $share = $share->where('action', $action_sh);
        $share = $share->first();

        if(!empty($share)){
            $share->count = $share->count+1;
            $share->save();
        }else{
            $newshare            = new Share;
            $newshare->action    = $action_sh;
            $newshare->count     = 1;
            $newshare->day       = date("d");
            $newshare->month     = date("F");
            $newshare->year      = date("Y");
            $newshare->save();
        }

		return ['status' => 'success'];
	}

	public function postAddRate(){

		$review_text 	= \Input::has('review_text') ? \Input::get('review_text') : "";
		$point 			= \Input::has('point') ? \Input::get('point') : "";
		$review_date 	= \Input::has('review_date') ? \Input::get('review_date') : "";
		$id 			= \Input::has('id') ? \Input::get('id') : "";
		$user_id 		= \Input::has('user_id') ? \Input::get('user_id') : "";

		$user_obj = \Session::has('current_user') ? \Session::get('current_user') : '';
		$user_id  = empty($user_obj->user_id) ? "" : $user_obj->user_id;

		$date = date('Y-m-d');
		// sd($date);

		$review 				= new Review;
		$review->review_text 	= $review_text;
		$review->point 			= $point;
		$review->review_date 	= $date;
		$review->place_id 		= $id;
		$review->user_id 		= $user_id;
		$review->save();
		
		return ['status' => 'success'];
	}


	public function ajaxCenter(){
		$method  		= \Input::has('method') ? \Input::get('method') : '';

		switch ($method) {
			case 'clearFilterToSession' :

				\Session::forget('report_filters');

				return ['status' => 'success'];
				break;

			case 'addFilterToSession':

				// รับค่ามาเพื่อ ค้นหาข้อมูล ในดาต้าเบส
				$title = \Input::has('title') ? \Input::get('title') : "";
				
				// สร้างเงื่อนไขเพื่อ query และส่งข้อมูลกลับไปสร้างmarker บน map
				$data = array();
				if($title == "ป้ายรถเมล์" || $title == "วินมอเตอร์ไซค์" || $title == "คิวรถตู้" || $title == "shuttle"){

					$places = Transport::where('transport_status', 'Active');
					$places = $places->where(function($places) use ($title){
									$places->where('transport_name','like', '%'.$title.'%');
									$places->orWhere('transport_description', 'like', '%'.$title.'%');
									$places->orWhere('type_transport', 'like', '%'.$title.'%');
						});

					$places = $places->get();

					foreach ($places as $key => $tran) {

						$data_arr = [$tran->transport_name, $tran->transport_lat, $tran->transport_long, $tran->transport_description, $tran->date_variable, $tran->time_open, $tran->transport_logoimage, $tran->type_transport];

						array_push($data, $data_arr);
					}

				}elseif($title == "ร้านอาหาร" || $title == "โรงแรม" || $title == "ห้างสรรพสินค้า" || $title == "ร้านกาแฟ" || $title == "สถานที่ออกกำลังกาย" || $title == "วัด" || $title == "สถานบันเทิง" || $title == "จุดแสดงสินค้า" || $title == "สถานศึกษา"){

					$places = Place::where('place_status', 'Active');
					$places = $places->where(function($places) use ($title){
									$places->where('place_name','like', '%'.$title.'%');
									$places->orWhere('place_description', 'like', '%'.$title.'%');
									$places->orWhere('place_soi', 'like', '%'.$title.'%');
									$places->orWhere('place_booth', 'like', '%'.$title.'%');
									$places->orWhere('type_place', 'like', '%'.$title.'%');
						});

					$places = $places->get();

					// sd($places->toArray());

					foreach ($places as $key => $place) {

						// $reviews = Review::where('place_id',$place->place_id)->get();
						$data_arr = [$place->place_name, $place->place_lat, $place->place_long, $place->place_description, $place->date_variable, $place->time_open, $place->place_logoimage, $place->type_place,$place->place_id];

						array_push($data, $data_arr);
					}

				}else{

					// ค้นหาโดยใช้ช่อง search
					$places = Place::where('place_status', 'Active');
					$places = $places->where(function($places) use ($title){
									$places->where('place_name','like', '%'.$title.'%');
									$places->orWhere('place_description', 'like', '%'.$title.'%');
									$places->orWhere('place_soi', 'like', '%'.$title.'%');
									$places->orWhere('place_booth', 'like', '%'.$title.'%');
									$places->orWhere('type_place', 'like', '%'.$title.'%');
						});

					$places = $places->get();

					// d($title);
					// sd($places->toArray());

					foreach ($places as $key => $place) {

						$data_arr = [$place->place_name, $place->place_lat, $place->place_long, $place->place_description, $place->date_variable, $place->time_open, $place->place_logoimage,$place->type_place,$place->place_id];

						array_push($data, $data_arr);
					}


				}

				return ['status' => 'success', 'data' => $places, 'datamark' => $data];
				break;

			case 'getModalReview' :

				$id = \Input::has('id') ? \Input::get('id') : "";

				if(empty($id)) return ['status' => 'error', 'msg' => 'Not found Data.'];

				$reviews = Review::with(['user','place'])->where('place_id', $id)->get();

				// sd($reviews->toArray());


				return ['status' => 'success', 'data' => $reviews];
				break;


			case 'getDataToSelected' :

				$places = Place::where('place_status', 'Active')->get();
				$trans = Transport::where('transport_status', 'Active')->get();

				// a = {value : {lat: 121, lng: 121}, text : "sdsds"}
				$data = array();

				foreach ($places as $key => $place) {

					$data_arr = $place->place_name;
					
					array_push($data, $data_arr);
				}


				foreach ($trans as $key => $tran) {

						$data_arr = $tran->transport_name;
						
						array_push($data, $data_arr);
					}

				// d($data);
				// d($places->toArray());
				// sd($trans->toArray());
				// sd();

				return ['status' => 'success', 'data' => $data];
				break;

			case 'getDataToMap' :

				$start = \Input::has('start') ? \Input::get('start') : "";
				$end = \Input::has('end') ? \Input::get('end') : "";

				// $places = Place::where('place_status', 'Active')->first();
				// $trans = Transport::where('transport_status', 'Active')->first();
				$data = array();

				if(!empty($start)){

					$places = Place::where('place_status', 'Active');
					$places = $places->where(function($places) use ($start){
									$places->where('place_name','like', '%'.$start.'%');
									$places->orWhere('place_description', 'like', '%'.$start.'%');
									$places->orWhere('place_soi', 'like', '%'.$start.'%');
									$places->orWhere('place_booth', 'like', '%'.$start.'%');
									$places->orWhere('type_place', 'like', '%'.$start.'%');
						});

					$places = $places->first();


					$trans = Transport::where('transport_status', 'Active');
					$trans = $trans->where(function($trans) use ($start){
										$trans->where('transport_name','like', '%'.$start.'%');
										$trans->orWhere('transport_description', 'like', '%'.$start.'%');
										$trans->orWhere('type_transport', 'like', '%'.$start.'%');
							});

					$trans = $trans->first();

					if(!empty($places)){
						$data_arrdata_arr['start'] = $places->place_lat.','.$places->place_long;
						array_push($data, $data_arrdata_arr);
					}

					if(!empty($trans)){
						$data_arr['start'] = $trans->transport_lat.','.$trans->transport_long;
						array_push($data, $data_arr);
					}
				}


				if(!empty($end)){

					$places = Place::where('place_status', 'Active');
					$places = $places->where(function($places) use ($end){
									$places->where('place_name','like', '%'.$end.'%');
									$places->orWhere('place_description', 'like', '%'.$end.'%');
									$places->orWhere('place_soi', 'like', '%'.$end.'%');
									$places->orWhere('place_booth', 'like', '%'.$end.'%');
									$places->orWhere('type_place', 'like', '%'.$end.'%');
						});

					$places = $places->first();


					$trans = Transport::where('transport_status', 'Active');
					$trans = $trans->where(function($trans) use ($end){
										$trans->where('transport_name','like', '%'.$end.'%');
										$trans->orWhere('transport_description', 'like', '%'.$end.'%');
										$trans->orWhere('type_transport', 'like', '%'.$end.'%');
							});

					$trans = $trans->first();

					if(!empty($places)){
						$data_arrdata_arr['end'] = $places->place_lat.','.$places->place_long;
						array_push($data, $data_arrdata_arr);
					}

					if(!empty($trans)){
						$data_arr['end'] = $trans->transport_lat.','.$trans->transport_long;
						array_push($data, $data_arr);
					}
				}

				// d($data);
				// d($data_arr);
				// d($places);
				// sd($trans);
				// sd();

				return ['status' => 'success', 'data' => $data];
				break;

			case 'getRouteForSearch' :

				$title = \Input::has('title') ? \Input::get('title') : "";

				$data = array();


				if(!empty($title)){

					$places = Place::where('place_status', 'Active');
					$places = $places->where(function($places) use ($title){
									$places->where('place_name','like', '%'.$title.'%');
									$places->orWhere('place_description', 'like', '%'.$title.'%');
									$places->orWhere('place_soi', 'like', '%'.$title.'%');
									$places->orWhere('place_booth', 'like', '%'.$title.'%');
									$places->orWhere('type_place', 'like', '%'.$title.'%');
						});

					$places = $places->first();


					$trans = Transport::where('transport_status', 'Active');
					$trans = $trans->where(function($trans) use ($title){
										$trans->where('transport_name','like', '%'.$title.'%');
										$trans->orWhere('transport_description', 'like', '%'.$title.'%');
										$trans->orWhere('type_transport', 'like', '%'.$title.'%');
							});

					$trans = $trans->first();

					if(!empty($places)){
						$data_arrdata_arr['start'] = $places->place_lat.','.$places->place_long;
						array_push($data, $data_arrdata_arr);
					}

					if(!empty($trans)){
						$data_arr['start'] = $trans->transport_lat.','.$trans->transport_long;
						array_push($data, $data_arr);
					}
				}


				return ['status' => 'success', 'data' => $data];
				break;

			default:
				return ['status' => 'error', 'msg' => 'Not found method'];
				break;
		}

	}

}
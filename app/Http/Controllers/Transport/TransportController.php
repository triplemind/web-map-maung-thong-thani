<?php

namespace App\Http\Controllers\Transport;

// use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Test\Test;
use App\Services\Users\User;
use App\Services\Place\Place;
use App\Services\PlaceType\PlaceType;
use App\Services\PlaceTypeDescription\PlaceTypeDescription;
use App\Services\Review\Review;
use App\Services\Share\Share;
use App\Services\Transport\Transport;
use App\Services\TransportType\TransportType;
use App\Services\TransportTypeDescription\TransportTypeDescription;

class TransportController extends Controller
{
	public function Index()
	{
		$transports = Transport::where('transport_id', '!=', 0)->get();

		return $this->view('manage_data.transport.index', compact('transports'));
	}

	public function getAdd()
	{

		return $this->view('manage_data.transport.add');
	}

	public function getEdit(Request $request, $id)
	{
		$transport = Transport::where('transport_id', $id)->first();

		return $this->view('manage_data.transport.edit', compact('transport'));
	}

	public function postAdd(){

		$user_obj = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_id  = empty($user_obj->user_id) ? "" : $user_obj->user_id;

        $transport_name 		= \Input::has('transport_name') ? \Input::get('transport_name') : '';
        $transport_description 	= \Input::has('transport_description') ? \Input::get('transport_description') : '';
        $date_variable 			= \Input::has('date_variable') ? \Input::get('date_variable') : '';
        $time_open 				= \Input::has('time_open') ? \Input::get('time_open') : '';
        $transport_status 		= \Input::has('transport_status') ? \Input::get('transport_status') : '';
        $transport_lowprice 	= \Input::has('transport_lowprice') ? \Input::get('transport_lowprice') : '';
        $transport_hightprice 	= \Input::has('transport_hightprice') ? \Input::get('transport_hightprice') : '';
        $type_transport 		= \Input::has('type_transport') ? \Input::get('type_transport') : '';
        $transport_logoimage 	= \Input::has('transport_logoimage') ? \Input::get('transport_logoimage') : '';
        $transport_description_id = \Input::has('transport_description_id') ? \Input::get('transport_description_id') : '';
        $transport_lat 			= \Input::has('transport_lat') ? \Input::get('transport_lat') : '';
        $transport_long 		= \Input::has('transport_long') ? \Input::get('transport_long') : '';

        $transport_logoimage 	= \Input::hasFile('transport_logoimage') ? \Input::file('transport_logoimage') : '';

        $foldername = "0000".$user_id;
        $date = date('Y-m-d');
        $transport_image_name = "";

        $path = base_path();
        if(!file_exists($path."/public/image_trans")){
            $oldmask = umask(0);
            mkdir($path."/public/image_trans", 0777);
            umask($oldmask);
        }

        if(!empty($transport_logoimage)){
           
            $transport_image_name = "/public/image_trans/".$date."-".$transport_logoimage->getClientOriginalName();
            copy($transport_logoimage, $path.$transport_image_name);
                
        }



        $transport 								= new Transport;
		$transport->transport_name 				= $transport_name;
		$transport->transport_description 		= $transport_description;
		$transport->time_open 					= $time_open;
		$transport->transport_status 			= $transport_status;
		$transport->date_variable 				= $date_variable;
		$transport->transport_lowprice 			= $transport_lowprice;
		$transport->transport_hightprice 		= $transport_hightprice;
		$transport->type_transport 				= $type_transport;
		$transport->transport_description_id 	= 0;
		$transport->transport_lat 				= $transport_lat;
		$transport->transport_long 				= $transport_long;
        $transport->transport_logoimage         = $transport_image_name;
		$transport->user_id 				    = $user_id;
		$transport->save();



		return ['status' => 'success'];

	}


	public function postEdit(){

		$user_obj = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_id  = empty($user_obj->user_id) ? "" : $user_obj->user_id;

        $transport_name 		= \Input::has('transport_name') ? \Input::get('transport_name') : '';
        $transport_description 	= \Input::has('transport_description') ? \Input::get('transport_description') : '';
        $date_variable 			= \Input::has('date_variable') ? \Input::get('date_variable') : '';
        $time_open 				= \Input::has('time_open') ? \Input::get('time_open') : '';
        $transport_status 		= \Input::has('transport_status') ? \Input::get('transport_status') : '';
        $transport_lowprice 	= \Input::has('transport_lowprice') ? \Input::get('transport_lowprice') : '';
        $transport_hightprice 	= \Input::has('transport_hightprice') ? \Input::get('transport_hightprice') : '';
        $type_transport 		= \Input::has('type_transport') ? \Input::get('type_transport') : '';
        $transport_description_id = \Input::has('transport_description_id') ? \Input::get('transport_description_id') : '';
        $transport_lat 			= \Input::has('transport_lat') ? \Input::get('transport_lat') : '';
        $transport_long 		= \Input::has('transport_long') ? \Input::get('transport_long') : '';
        $id 					= \Input::has('id') ? \Input::get('id') : '';

        $transport_logoimage 	= \Input::hasFile('transport_logoimage') ? \Input::file('transport_logoimage') : '';

        $arrtransport = Transport::where('transport_id', $id)->first();
        if(empty($arrtransport)) return helperReturnErrorFormRequest('Not found Data.');


        $foldername = "0000".$user_id;
        $date = date('Y-m-d');
        $transport_image_name = "";

        $path = base_path();
        if(!file_exists($path."/public/image_trans")){
            $oldmask = umask(0);
            mkdir($path."/public/image_trans", 0777);
            umask($oldmask);
        }

        if(!empty($transport_logoimage)){
           
            $transport_image_name = "/public/image_trans/".$date."-".$transport_logoimage->getClientOriginalName();
            copy($transport_logoimage, $path.$transport_image_name);
                
        }

        // sd($place_image_name);

		$arrtransport->transport_name 				= $transport_name;
		$arrtransport->transport_description 		= $transport_description;
		$arrtransport->time_open 					= $time_open;
		$arrtransport->transport_status 			= $transport_status;
		$arrtransport->date_variable 				= $date_variable;
		$arrtransport->transport_lowprice 			= $transport_lowprice;
		$arrtransport->transport_hightprice 		= $transport_hightprice;
		if(!empty($transport_image_name)){

			$arrtransport->transport_logoimage 		= $transport_image_name;
		}
		$arrtransport->type_transport 				= $type_transport;
		$arrtransport->transport_description_id 	= 0;
		$arrtransport->transport_lat 				= $transport_lat;
		$arrtransport->transport_long 				= $transport_long;
		$arrtransport->user_id 						= $user_id;
		$arrtransport->save();



		return ['status' => 'success'];

	}


	public function postRemove(){

		$transport_id = \Input::has('transport_id') ? \Input::get('transport_id') : '';
       
        $transport    = Transport::where('transport_id', $transport_id)->first();

        if(empty($transport)) return helperReturnErrorFormRequest('Not found Data.');

        $transport->delete();

		return ['status' => 'success'];

	}


	public function ajaxCenter(){



		return ['status' => 'success'];

	}

}
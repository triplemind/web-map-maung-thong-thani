<?php

namespace App\Http\Controllers\ServiceData;

// use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Test\Test;
use App\Services\Users\User;
use App\Services\Place\Place;
use App\Services\PlaceType\PlaceType;
use App\Services\PlaceTypeDescription\PlaceTypeDescription;
use App\Services\Review\Review;
use App\Services\Share\Share;
use App\Services\Transport\Transport;
use App\Services\TransportType\TransportType;
use App\Services\TransportTypeDescription\TransportTypeDescription;

class ServiceDataController extends Controller
{
	public function Index()
	{
		// get ข้อมูลไปแสดงหน้า view (ตาราง)
		$places = Place::where(function($places){
						$places->where('type_place','ธนาคาร');
						$places->orWhere('type_place', 'ห้องน้ำ');
						$places->orWhere('type_place', 'จุดจอดรถ');
						$places->orWhere('type_place', 'ที่ทำการไปรษณีย์');
				});
		$places = $places->get();


		return $this->view('manage_data.service.index', compact('places'));
	}

	
	public function getAdd()
	{
		// เรียกวิวหน้า Add ข้อมูลของจุดอำนวยความสะดวก
		return $this->view('manage_data.service.add');
	}

	public function getEdit(Request $request, $id)
	{
		// เรียกวิวหน้า Edit และส่งข้อมูลไปแสดง 
		$place = Place::where('place_id',$id)->first();

		return $this->view('manage_data.service.edit',compact('place'));
	}

	public function postAdd(){

		// get ข้อมูลมาเก็บลงดาต้าเบส
		$user_obj = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_id  = empty($user_obj->user_id) ? "" : $user_obj->user_id;

		$place_name 		= \Input::has('place_name') ? \Input::get('place_name') : '';
		$place_description 	= \Input::has('place_description') ? \Input::get('place_description') : '';
		$date_variable 		= \Input::has('date_variable') ? \Input::get('date_variable') : '';
		$time_open 			= \Input::has('time_open') ? \Input::get('time_open') : '';
		$place_tel 			= \Input::has('place_tel') ? \Input::get('place_tel') : '';
		$place_status 		= \Input::has('place_status') ? \Input::get('place_status') : '';
		$place_soi 			= \Input::has('place_soi') ? \Input::get('place_soi') : '';
		$place_booth 		= \Input::has('place_booth') ? \Input::get('place_booth') : '';
		$place_lat 			= \Input::has('place_lat') ? \Input::get('place_lat') : '';
		$place_long 		= \Input::has('place_long') ? \Input::get('place_long') : '';
		$type_place 		= \Input::has('type_place') ? \Input::get('type_place') : '';
		$place_logoimage 	= \Input::hasFile('place_logoimage') ? \Input::file('place_logoimage') : '';
		$place_allimage 	= \Input::hasFile('place_allimage') ? \Input::file('place_allimage') : '';

		// เริ่มต้น สร้างโฟลเดอร์เพื่อเก็บรูปภาพ
		$foldername = "0000".$user_id;
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/image_service")){
            $oldmask = umask(0);
            mkdir($path."/public/image_service", 0777);
            umask($oldmask);
        }

        $place_image_name = "";
        if(!empty($place_logoimage)){
           
            $place_image_name = "/public/image_service/".$date."-".$place_logoimage->getClientOriginalName();
            copy($place_logoimage, $path.$place_image_name);
                
        }
        //สิ้นสุด  สร้างโฟลเดอร์เพื่อเก็บรูปภาพ


        // ADD ข้อมูลลงดาต้าเบส
		$place 							= new Place;
		$place->place_name 				= $place_name;
		$place->place_description 		= $place_description;
		$place->date_variable 			= $date_variable;
		$place->time_open 				= $time_open;
		$place->place_tel 				= $place_tel;
		$place->place_status 			= $place_status;
		$place->place_soi 				= $place_soi;
		$place->place_booth 			= $place_booth;
		$place->place_lat 				= $place_lat;
		$place->place_long 				= $place_long;
		$place->place_logoimage 		= $place_image_name;
		$place->type_place 				= $type_place;
		$place->place_description_id 	= 0;
		$place->user_id 				= $user_id;
		$place->save();

        
		// เริ่มต้น สร้างโฟลเดอร์เพื่อเก็บรูปภาพอื่นๆ
        if(!empty($place_allimage)){
            if(!file_exists($path."/public/image_service/all_img")){
                $oldmask = umask(0);
                mkdir($path."/public/image_service/all_img", 0777);
                umask($oldmask);
            }

            $all_image_name = "";
            foreach($place_allimage as $key => $imgall) {

                if(!empty($imgall))
                {
                    $all_image_name = "/public/image_service/all_img/".$date."-".$imgall->getClientOriginalName();
                    copy($imgall, $path.$all_image_name);
                }

                $all_img 							= new PlaceTypeDescription;
				$all_img->place_description_name 	= $place->place_name;
				$all_img->place_description_image 	= $all_image_name;
				$all_img->save();
            }
        }
		//สิ้นสุด  สร้างโฟลเดอร์เพื่อเก็บรูปภาพอื่นๆ

		return ['status' => 'success'];

	}


	public function postEdit(){
$user_obj = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_id  = empty($user_obj->user_id) ? "" : $user_obj->user_id;

		$id 				= \Input::has('id') ? \Input::get('id') : '';
		$place_name 		= \Input::has('place_name') ? \Input::get('place_name') : '';
		$place_description 	= \Input::has('place_description') ? \Input::get('place_description') : '';
		$date_variable 		= \Input::has('date_variable') ? \Input::get('date_variable') : '';
		$time_open 			= \Input::has('time_open') ? \Input::get('time_open') : '';
		$place_tel 			= \Input::has('place_tel') ? \Input::get('place_tel') : '';
		$place_status 		= \Input::has('place_status') ? \Input::get('place_status') : '';
		$place_soi 			= \Input::has('place_soi') ? \Input::get('place_soi') : '';
		$place_booth 		= \Input::has('place_booth') ? \Input::get('place_booth') : '';
		$place_lat 			= \Input::has('place_lat') ? \Input::get('place_lat') : '';
		$place_long 		= \Input::has('place_long') ? \Input::get('place_long') : '';
		$type_place 		= \Input::has('type_place') ? \Input::get('type_place') : '';
		$place_logoimage 	= \Input::hasFile('place_logoimage') ? \Input::file('place_logoimage') : '';
		$place_allimage 	= \Input::hasFile('place_allimage') ? \Input::file('place_allimage') : '';
       

        $place = Place::where('place_id', $id)->first();
        if(empty($place)) return helperReturnErrorFormRequest('Not found Data.');


		$foldername = "0000".$user_id;
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/image_service")){
            $oldmask = umask(0);
            mkdir($path."/public/image_service", 0777);
            umask($oldmask);
        }

        $place_image_name = "";
        if(!empty($place_logoimage)){
           
            $place_image_name = "/public/image_service/".$date."-".$place_logoimage->getClientOriginalName();
            copy($place_logoimage, $path.$place_image_name);
                
        }


		$place->place_name 				= $place_name;
		$place->place_description 		= $place_description;
		$place->date_variable 			= $date_variable;
		$place->time_open 				= $time_open;
		$place->place_tel 				= $place_tel;
		$place->place_status 			= $place_status;
		$place->place_soi 				= $place_soi;
		$place->place_booth 			= $place_booth;
		$place->place_lat 				= $place_lat;
		$place->place_long 				= $place_long;
		if(!empty($place_image_name)){

			$place->place_logoimage 		= $place_image_name;
		}
		$place->type_place 				= $type_place;
		$place->place_description_id 	= 0;
		$place->user_id 				= $user_id;
		$place->save();

        if(!empty($place_allimage)){
            if(!file_exists($path."/public/image_service/all_img")){
                $oldmask = umask(0);
                mkdir($path."/public/image_service/all_img", 0777);
                umask($oldmask);
            }

            $all_image_name = "";
            foreach($place_allimage as $key => $imgall) {

                if(!empty($imgall))
                {
                    $all_image_name = "/public/image_service/all_img/".$date."-".$imgall->getClientOriginalName();
                    copy($imgall, $path.$all_image_name);
                }

                $all_img 							= new PlaceTypeDescription;
				$all_img->place_description_name 	= $place->place_name;
				$all_img->place_description_image 	= $all_image_name;
				$all_img->save();
            }
        }


		return ['status' => 'success'];

	}


	public function postRemove(){

		$place_id = \Input::has('place_id') ? \Input::get('place_id') : '';
       
        $place    = Place::where('place_id', $place_id)->first();

        if(empty($place)) return helperReturnErrorFormRequest('Not found Data.');

        $place->delete();

		return ['status' => 'success'];

	}


	public function ajaxCenter(){



		return ['status' => 'success'];

	}


}
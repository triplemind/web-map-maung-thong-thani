<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;


class UserFormRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'username'      => 'required|unique:tbl_users',
            'password'      => 'required',
            'email'     	=> 'required|email|unique:tbl_users,email',
            'user_type'     => 'required',
            'first_name'    => 'required',
            'last_name'     => 'required',
        ];
    }

    public function messages()
    {
        return [
            'username.required'     => 'กรุณากรอก Username',
            'username.unique'       => 'Username นี้มีอยู่ในระบบแล้ว',
            'password.required'     => 'กรุณากรอกรหัสผ่าน',
            'email.required'        => 'กรุณากรอก Email',
            'email.email'           => 'กรุณากรอกตรวจสอบรูปแบบของ Email',
            'email.unique'          => 'Email นี้มีอยู่ในระบบแล้ว',
            'user_type.required'    => 'กรุณากรอกเลือกประเภทของผู้ใช้งาน',
            'first_name.required'   => 'กรุณากรอกชื่อ',
            'last_name.required'    => 'กรุณากรอกนามสกุล',
        ];
    }
}
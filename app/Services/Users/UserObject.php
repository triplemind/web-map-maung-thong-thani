<?php namespace App\Services\Users;

use App\Services\Users\User;

class UserObject {

	protected $user_id;
	protected $username;
	protected $user_type;
	protected $email;
	protected $user_img;

	public function __construct()
	{
		// Put Session to Object.
		if (\Session::has('current_user')) {

			$current_user 		= \Session::get('current_user');
			$this->user_id		= $current_user->user_id;
			$this->username 	= $current_user->username;
			$this->first_name 	= $current_user->first_name;
			$this->last_name 	= $current_user->last_name;
			$this->user_type 	= $current_user->user_type;
			$this->email 		= $current_user->email;
			$this->user_img 	= $current_user->user_img;
		}
	}

	public function setUp($userModel)
	{
		// Save to Session.
        \Session::put('current_user', $userModel);
    }

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function getEmail()
	{
		return $this->email;
	}
	public function getUsername()
	{
		return $this->username;
	}
	public function getFirstname()
	{
		return $this->first_name;
	}
	public function getLastname()
	{
		return $this->last_name;
	}
	public function getUserType()
	{
		return $this->user_type;
	}
	public function getUserImage()
	{
		return $this->user_img;
	}
	

}
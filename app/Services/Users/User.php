<?php namespace App\Services\Users;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'tbl_users';
 	protected $primaryKey 	= 'user_id';

}
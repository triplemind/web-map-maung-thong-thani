<?php namespace App\Services\PlaceType;

use Illuminate\Database\Eloquent\Model;

class PlaceType extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'type_place';
 	protected $primaryKey 	= 'type_place_id';

}
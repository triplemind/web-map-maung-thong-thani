<?php namespace App\Services\Share;

use Illuminate\Database\Eloquent\Model;

class Share extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'share';
 	protected $primaryKey 	= 'share_id';

}
<?php namespace App\Services\TransportType;

use Illuminate\Database\Eloquent\Model;

class TransportType extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'type_transport';
 	protected $primaryKey 	= 'type_transport_id';

}
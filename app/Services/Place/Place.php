<?php namespace App\Services\Place;

use Illuminate\Database\Eloquent\Model;

class Place extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'place';
 	protected $primaryKey 	= 'place_id';

}
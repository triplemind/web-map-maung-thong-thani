<?php namespace App\Services\Transport;

use Illuminate\Database\Eloquent\Model;

class Transport extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'transport';
 	protected $primaryKey 	= 'transport_id';

}
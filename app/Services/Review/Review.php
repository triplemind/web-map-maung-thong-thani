<?php namespace App\Services\Review;

use Illuminate\Database\Eloquent\Model;

class Review extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'review';
 	protected $primaryKey 	= 'review_id';


 	public function user()
	{
		return $this->belongsTo("App\Services\Users\User", 'user_id');
	}

	public function place()
	{
		return $this->belongsTo("App\Services\Place\Place", 'place_id');
	}

}
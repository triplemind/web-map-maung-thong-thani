<?php namespace App\Services\PlaceTypeDescription;

use Illuminate\Database\Eloquent\Model;

class PlaceTypeDescription extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'type_place_description';
 	protected $primaryKey 	= 'place_description_id';

}
<?php namespace App\Services\TransportTypeDescription;

use Illuminate\Database\Eloquent\Model;

class TransportTypeDescription extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'type_transport_description';
 	protected $primaryKey 	= 'type_transport_description_id';

}
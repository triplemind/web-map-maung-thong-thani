<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Dashboard">
	<meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<title>ระบบช่วยเหลือการเดินทางในเมืองทอง</title>

	<!-- Favicons -->
	<link href="/themes/img/favicon.png" rel="icon">
	<link href="/themes/img/apple-touch-icon.png" rel="apple-touch-icon">

	<!-- Bootstrap core CSS -->
	<link href="/themes/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!--external css-->
	<link href="/themes/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<!-- Custom styles for this template -->
	<link href="/themes/css/style.css" rel="stylesheet">
	<link href="/themes/css/style-responsive.css" rel="stylesheet">
 
</head>

<body>
  
	<div id="login-page">
		<div class="container">
			<form class="form-login"  method="POST">
				<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
				<h2 class="form-login-heading">Login now</h2>
				<div class="login-wrap">
					<input type="text" name="username" class="form-control" placeholder="Username" required autofocus value="<?php //echo \Input::old("username") ?>">
					<br>
					<input type="password" name="password" class="form-control" required placeholder="Password">
					<br>
					<button id="btn-login" class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>

          <button id="btn-regist" class="btn btn-theme btn-block" type="submit" onclick="window.location.href = '/auth/register'; "> REGISTER</button>

          <button id="btn-back" class="btn btn-theme02 btn-block" type="submit" onclick="window.location.href = '/'; "> BACK</button>
				
				</div>

				<div class="row">
                    <div class="col-xs-12 p-t-5">
                     <!-- Error Validation -->
                    <?php if (count($errors) > 0): ?>
                        <div class="alert alert-danger alert-dismissible text-center">
                            <?php foreach ($errors->all() as $error): ?>
                                <?php echo $error ?>
                            <?php endforeach ?>
                        </div>
                    <?php endif ?>
                    </div>
                </div>
			
			</form>
		</div>
	</div>

	<div id="url-login" data-url_login="<?php echo \URL::route('auth.login.post'); ?>"></div>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="/themes/lib/jquery/jquery.min.js"></script>
  <script src="/themes/lib/bootstrap/js/bootstrap.min.js"></script>
  <!--BACKSTRETCH-->
  <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
  <script type="text/javascript" src="/themes/lib/jquery.backstretch.min.js"></script>
  <script>
    // $.backstretch("/themes/img/BG_test.png", {
    $.backstretch("/themes/img/gbgh.jpg", {
      speed: 500
    });

    $(function () {
    // $('#sign_in').validate({
    //     highlight: function (input) {
    //         console.log(input);
    //         $(input).parents('.form-line').addClass('error');
    //     },
    //     unhighlight: function (input) {
    //         $(input).parents('.form-line').removeClass('error');
    //     },
    //     errorPlacement: function (error, element) {
    //         $(element).parents('.input-group').append(error);
    //     }
    // });

    $('#btn-login').on('click', function(){
    	var username = $('input[name=username]').val();
    	var password = $('input[name=password]').val();
    	var url_login = $('#url-login').data(url_login);
    	
    	console.log(username + password + url_login);

		$.ajax({
			type: "POST",
			url: url_login,
			data: {username : username, password : password},
			success: function(Response) {

			}
		});


    });


});
  </script>
</body>

</html>

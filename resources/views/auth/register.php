<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Dashboard">
	<meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<title>ระบบช่วยเหลือการเดินทางในเมืองทอง</title>

	<!-- Favicons -->
	<link href="/themes/img/favicon.png" rel="icon">
	<link href="/themes/img/apple-touch-icon.png" rel="apple-touch-icon">

	<!-- Bootstrap core CSS -->
	<link href="/themes/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!--external css-->
	<link href="/themes/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<!-- Custom styles for this template -->
	<link href="/themes/css/style.css" rel="stylesheet">
	<link href="/themes/css/style-responsive.css" rel="stylesheet">

	<!-- Sweetalert Css -->
    <link href="/themes/lib/sweetalert/sweetalert.css" rel="stylesheet" />
 
</head>

<body>
  
	<div>
		<div class="container">
			<div class="form-login">
				
				<h2 class="form-login-heading">sign in now</h2>
				<div class="login-wrap">
					<input type="text" name="username" class="form-control" placeholder="Enter your Username. ">
					<br>

					<input type="password" name="password" class="form-control" placeholder="Enter your Password">
					<br>

					<input type="password" name="confirm_password" class="form-control" placeholder="Enter Confirm Password">
					<p id='message' style="margin: unset;"></p>
					<br>

					<input type="text" name="Firstname" class="form-control" placeholder="Enter your Firstname. ">
					<br>

					<input type="text" name="Lastname" class="form-control" placeholder="Enter your Lastname. ">
					<br>

					<input type="text" name="Email" class="form-control" placeholder="Enter your Email. ">
					<br>

					<button id="btn-singin" class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>

					<!-- <button class="btn btn-theme02 btn-block" type="submit" onclick="window.location.href = '/'; "> BACK</button> -->
				
				</div>
			
			</div>
		</div>
	</div>

	<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
	<div id="url_register" data-url="<?php echo \URL::route('auth.register.post'); ?>"></div>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="/themes/lib/jquery/jquery.min.js"></script>
  <script src="/themes/lib/bootstrap/js/bootstrap.min.js"></script>

  <script src="/js/site.js"></script>
  <!--BACKSTRETCH-->
  <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
  <script type="text/javascript" src="/themes/lib/jquery.backstretch.min.js"></script>

  <!-- Jquery Sweetalert Js -->
    <script src="/themes/lib/sweetalert/sweetalert.min.js"></script>

    <!-- Bootbox Plugin Js -->
    <script src="/themes/lib/bootstrap-bootbox/bootbox.min.js"></script>
	
  <script>
    // $.backstretch("/themes/img/BG_test.png", {
    $.backstretch("/themes/img/gbgh.jpg", {
      speed: 500
    });

    $(function () {
		$('input[name=password], input[name=confirm_password]').on('keyup', function () {
			if ($('input[name=password]').val() == $('input[name=confirm_password]').val()) {
				$('#message').html('Confirm Password Matching').css('color', 'green');
				$('#message').css('margin', '5px');
			} else 
				$('#message').html('Confirm Password Not Matching !!!').css('color', 'red');
				$('#message').css('margin', '5px');
		});



		$('#btn-singin').on('click',function(){
	        msg_waiting();
	        // window.location = 

	        var username        = $('input[name=username]').val();
	        var email           = $('input[name=Email]').val();
	        var password        = $('input[name=password]').val();
	        var user_type       = $('select[name=user_type]').val();
	        var first_name      = $('input[name=Firstname]').val();
	        var last_name       = $('input[name=Lastname]').val();
	        var add_url         = $('#url_register').data('url');
	        
	        // console.log(username);
	        // console.log(email);
	        // console.log(password);
	        // console.log(user_type);
	        // console.log(first_name);
	        // console.log(last_name);
	        // console.log(add_url);

	        // check format email
			var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

			if (testEmail.test(email)){
		        $.ajax({
		            headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		            type: 'post',
		            url: add_url,
		            data: {
		                'username'  : username,
		                'email'     : email,
		                'password'  : password,
		                'username'  : username,
		                'user_type' : user_type,
		                'first_name': first_name,
		                'last_name' : last_name,
		            },
		            success: function(result) {
		                if(result.status == "success"){
		                	window.location.href = "/auth/login";
		                }
		                if(result.status == "error"){
		                	console.log(result);
		                	msg_error_custom("Error", result.msg);
		                }
		            },
		        });
	        }else{
				msg_error_custom("Invalid Email", "Please check your email format.");
			}
	    });
	});
  </script>
</body>

</html>

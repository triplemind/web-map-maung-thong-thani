 <!--sidebar start-->
<aside>
	<div id="sidebar" class="nav-collapse " style="overflow-y: scroll;">
		<!-- sidebar menu start-->
		<ul class="sidebar-menu" id="nav-accordion">
			<!-- แสดงรูป และ ชื่อของผู้เข้าใช้งาน -->
			<?php if(\Session::has('current_user')):?>
				<?php 
					if(!empty(\Session::get('current_user')->user_img)){
						$image_show = str_replace("/public","",\Session::get('current_user')->user_img);
					} 
				?>
				<!-- href="<?php// echo \URL::route('profile.index.get');?>" -->
				<p class="centered"><a  class="btn-edit-profile" data-edit_url="<?php echo \URL::route('profile.index.get');?>" data-user_id="<?php echo \Session::get('current_user')->user_id; ?>"><img src="<?php echo (empty(\Session::get('current_user')->user_img))  ? url("")."/themes/img/not_found.png" : url("").$image_show; ?>" class="img-circle" width="80"></a></p>
				<h5 class="centered"><?php echo \Session::get('current_user')->username; ?></h5>
			<?php endif ?>

			<!-- ช่องค้นหา  -->
			<div class="input-group-append search-sub" style="text-align: end;margin: 10px;">
				<div style="display: none;" class="startp">
					<!-- <input type="text" name="start" class="form-control" placeholder="สถานที่ต้นทาง..."> -->
					<form autocomplete="off" action="/action_page.php">
	  					<div class="autocomplete" style="width:100%;">
							<input id="origin-input" class="controls form-control" type="text" placeholder="สถานที่ต้นทาง...">
								<span style="color: #fff;font-size: 16px;font-weight: 900;">
								<br>
								<br>
								</span>
							<!-- <input type="text" name="end" class="form-control" placeholder="สถานที่ปลายทาง...."> -->
		        			<input id="destination-input" class="controls form-control" type="text" placeholder="สถานที่ปลายทาง....">
		        		</div>
		        	</form>

				</div>
				
				<div class="autocomplete" style="width:100%;">
					<input type="text" id="searchonly" name="search" class="form-control" placeholder="ค้นหาสถานที่....">
				</div>

				<button id="btn-search-direction" class="btn btn-outline-secondary btn-success btn-search-direction" style="margin-top: 10px;display: none;" type="button">ค้นหา</button>
				<button class="btn btn-outline-secondary btn-success btn-direction" style="margin-top: 10px;" type="button">ค้นหาเส้นทาง</button>
				<button class="btn btn-outline-secondary btn-info btn-search" style="margin-top: 10px;" type="button">ค้นหา</button>
				<!-- ปุ่มเคลียร์ marker บน map -->
				<button class="btn btn-outline-secondary btn-danger del" type="button" value="Delete" style="margin-top: 10px;">ล้าง</button>	
			</div>
<!-- 
			<div id="mode-selector">
				<input class="form-control" type="radio" name="type" id="changemode-walking" checked="checked">
				<label for="changemode-walking">Walking</label>

				<input class="form-control" type="radio" name="type" id="changemode-transit">
				<label for="changemode-transit">Transit</label>

				<input class="form-control" type="radio" name="type" id="changemode-driving">
				<label for="changemode-driving">Driving</label>
			</div> -->


			<li class="mt">
				<a class="menu-map" data-toggle="modal" data-target="#myModal">
					<i class="fa fa-file-text-o"></i>
					<span>เกี่ยวกับฉัน</span>
				</a>
			</li>
			

			<li class="mt">
				<a class="menu-map" href="<?php echo \URL::route('dash.index.get');?>">
					<i class="fa fa-map-marker"></i>
					<span>Map</span>
				</a>
			</li>

			<li class="sub-menu">
				<a href="javascript:;">
					<i class="fa fa-desktop"></i>
					<span>สถานที่สำคัญ</span>
				</a>
				<ul class="sub">
					<li><a class="search-data" data-title="จุดแสดงสินค้า"><i class="fa fa-map-marker"></i>จุดแสดงสินค้า</a></li>
					<li><a class="search-data" data-title="ร้านอาหาร"><i class="fa fa-cutlery"></i>ร้านอาหาร</a></li>
					<li><a class="search-data" data-title="โรงแรม"><i class="fa fa-building-o"></i>โรงแรม</a></li>
					<li><a class="search-data" data-title="ห้างสรรพสินค้า"><i class="fa fa-shopping-cart"></i>ห้างสรรพสินค้า</a></li>
					<li><a class="search-data" data-title="สถานที่ออกกำลังกาย"><i class="fa fa-map-marker"></i>สถานที่ออกกำลังกาย</a></li>
					<li><a class="search-data" data-title="สถานบันเทิง"><i class="fa fa-glass"></i>สถานบันเทิง</a></li>
					<li><a class="search-data" data-title="วัด"><i class="fa fa-map-marker"></i>วัด</a></li>
					<li><a class="search-data" data-title="ร้านกาแฟ"><i class="fa fa-coffee"></i>ร้านกาแฟ</a></li>
					<li><a class="search-data" data-title="สถานศึกษา"><i class="fa fa-building-o"></i>สถานศึกษา</a></li>
				</ul>


				<!-- <ul class="" style="margin-top: 10px;">
					<button class="btn btn-outline-secondary btn-theme02 search-data" data-title="ร้านอาหาร" type="button" style="margin-bottom: 10px;padding: 0px 7px;">ร้านอาหาร</button>
					<button class="btn btn-outline-secondary btn-theme02 search-data" data-title="โรงแรม" type="button" style="margin-bottom: 10px;padding: 0px 7px;">โรงแรม</button>
					<button class="btn btn-outline-secondary btn-theme02 search-data" data-title="ห้างสรรพสินค้า" type="button" style="margin-bottom: 10px;padding: 0px 7px;">ห้างสรรพสินค้า</button>
					<button class="btn btn-outline-secondary btn-theme02 search-data" data-title="สถานที่ออกกำลังกาย" type="button" style="margin-bottom: 10px;padding: 0px 7px;">สถานที่ออกกำลังกาย</button>
					<button class="btn btn-outline-secondary btn-theme02 search-data" data-title="สถานบันเทิง" type="button" style="margin-bottom: 10px;padding: 0px 7px;">สถานบันเทิง</button>
					<button class="btn btn-outline-secondary btn-theme02 search-data" data-title="วัด" type="button" style="margin-bottom: 10px;padding: 0px 7px;">วัด</button>
					<button class="btn btn-outline-secondary btn-theme02 search-data" data-title="ร้านกาแฟ" type="button" style="margin-bottom: 10px;padding: 0px 7px;">ร้านกาแฟ</button>
					
				</ul> -->
			</li>

			<li class="sub-menu">
				<a href="javascript:;">
					<i class="fa fa-desktop"></i>
					<span>จุดอำนวยความสะดวก</span>
				</a>

				<ul class="sub">
					<li><a class="search-data" data-title="ธนาคาร"><i class="fa fa-money"></i>ธนาคาร</a></li>
					<li><a class="search-data" data-title="ที่ทำการไปรษณีย์"><i class="fa fa-envelope"></i>ที่ทำการไปรษณีย์</a></li>
					<li><a class="search-data" data-title="ห้องน้ำ"><i class="fa fa-map-marker"></i>ห้องน้ำ</a></li>
					<li><a class="search-data" data-title="จุดจอดรถ"><i class="fa fa-map-marker"></i>จุดจอดรถ</a></li>
				</ul>

				<!-- <ul class="" style="margin-top: 10px;">
					<button class="btn btn-outline-secondary btn-theme02 search-data" data-title="ธนาคาร" type="button" style="margin-bottom: 10px;padding: 0px 7px;">ธนาคาร</button>
					<button class="btn btn-outline-secondary btn-theme02 search-data" data-title="ห้องน้ำ" type="button" style="margin-bottom: 10px;padding: 0px 7px;">ห้องน้ำ</button>
					<button class="btn btn-outline-secondary btn-theme02 search-data" data-title="จุดจอดรถ" type="button" style="margin-bottom: 10px;padding: 0px 7px;">จุดจอดรถ</button>
				</ul> -->
			</li>

			<li class="sub-menu">
				<a href="javascript:;">
					<i class="fa fa-desktop"></i>
					<span>ระบบขนส่งสาธารณะ</span>
				</a>

				<ul class="sub">
					<li><a class="search-data" data-title="ป้ายรถเมล์"><i class="fa fa-map-marker"></i>ป้ายรถเมล์</a></li>
					<li><a class="search-data" data-title="วินมอเตอร์ไซค์"><i class="fa fa-map-marker"></i>วินมอเตอร์ไซค์</a></li>
					<li><a class="search-data" data-title="คิวรถตู้"><i class="fa fa-map-marker"></i>คิวรถตู้</a></li>
					<li><a class="search-data" data-title="shuttle"><i class="fa fa-map-marker"></i>รถ Shuttle bus</a></li>
				</ul>

				<!-- <ul class="" style="margin-top: 10px;">
					<button class="btn btn-outline-secondary btn-theme02 search-data" data-title="ป้ายรถเมล์" type="button" style="margin-bottom: 10px;padding: 0px 7px;">ป้ายรถเมล์</button>
					<button class="btn btn-outline-secondary btn-theme02 search-data" data-title="วินมอเตอร์ไซค์" type="button" style="margin-bottom: 10px;padding: 0px 7px;">วินมอเตอร์ไซค์</button>
					<button class="btn btn-outline-secondary btn-theme02 search-data" data-title="คิวรถตู้" type="button" style="margin-bottom: 10px;padding: 0px 7px;">คิวรถตู้</button>
					<button class="btn btn-outline-secondary btn-theme02 search-data" data-title="shuttle" type="button" style="margin-bottom: 10px;padding: 0px 7px;">รถ Shuttle bus</button>
				</ul> -->
			</li>

			<!-- เมนูสำหรับ admin -->
			<?php if(\Session::has('current_user')):?>
			<?php if(\Session::get('current_user')->user_type == 'admin'): ?>
				<li>
					<a class="menu-user" href="<?php echo \URL::route('user.index.get');?>">
						<i class="fa fa-edit"></i>
						<span>Manage User</span>
					</a>
				</li>

				<li class="sub-menu">
					<a href="javascript:;">
						<i class="fa fa-desktop"></i>
						<span>Manage Data</span>
					</a>
					<ul class="sub">
						<li><a class="menu-place" href="<?php echo \URL::route('place.index.get');?>">ข้อมูลสถานที่สำคัญ</a></li>
						<li><a class="menu-service" href="<?php echo \URL::route('service.index.get');?>">จุดอำนวยความสะดวก</a></li>
						<li><a class="menu-trans" href="<?php echo \URL::route('transport.index.get');?>">ข้อมูลระบบขนส่งสาธารณะ</a></li>
					</ul>
				</li>
			<?php endif ?>
			<?php endif ?>
		</ul>
		<!-- sidebar menu end-->

	</div>
</aside>
<!--sidebar end-->

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">เกี่ยวกับฉัน</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" style="padding: 0px 60px;color: #000;">
                        <div style="font-size: 18px;padding: 10px 0px;">
                        	ระบบช่วยเหลือการเดินทางในเมืองทอง เป็นเว็บไซต์ฐานข้อมูล Search Engine ที่ช่วยในการค้นหา จุดแสดงสินค้า จุดอำนวยความสะดวก จุดจอดรถโดยสาร ซึ่งจะสามารถค้นหาได้แค่เฉพาะภายในเมืองทองธานีเท่านั้น
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<style type="text/css">
	li a.active{
		background:#2f323a !important;
		color: #fff !important;
	}

	li a{
		cursor: pointer;
	}
</style>

<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id='ajax-center-url' data-url="<?php echo \URL::route('user.ajax_center.post');?>"></div>
<div id='edit-url' data-url="<?php echo \URL::route('user.edit.post');?>"></div>


<script type="text/javascript">
	$(function(){
		var pathname = window.location.pathname;
	    var title = pathname.replace("/","");

	    console.log(title);

	    if(title !== ""){
	    	$('.search-sub').css('display','none');

		    if(title == "user"){
		    	$('.menu-user').addClass('active');

		    	$('.menu-map').removeClass('active');
		    	$('.menu-place').removeClass('active');
		    	$('.menu-service').removeClass('active');
		    	$('.menu-trans').removeClass('active');

		    }else if(title == "place" || title == "place/add"){
		    	$('.sub').css('display','unset');
		    	$('.menu-place').css('color','#fff');
		    	$('.menu-place').addClass('active');

		    	$('.menu-map').removeClass('active');
		    	$('.menu-user').removeClass('active');
		    	$('.menu-service').removeClass('active');
		    	$('.menu-trans').removeClass('active');
		    }else if(title == "service"  || title == "service/add"){
		    	$('.sub').css('display','unset');
		    	$('.menu-place').css('color','#fff');
		    	$('.menu-service').addClass('active');

		    	$('.menu-map').removeClass('active');
		    	$('.menu-user').removeClass('active');
		    	$('.menu-place').removeClass('active');
		    	$('.menu-trans').removeClass('active');
		    }else if(title == "transport"  || title == "transport/add"){
		    	$('.sub').css('display','unset');
		    	$('.menu-place').css('color','#fff');
		    	$('.menu-trans').addClass('active');

		    	$('.menu-map').removeClass('active');
		    	$('.menu-user').removeClass('active');
		    	$('.menu-place').removeClass('active');
		    	$('.menu-service').removeClass('active');
		    }

	    }else{

	    	$('.menu-map').addClass('active');

	    	$('.menu-user').removeClass('active');
	    	$('.menu-place').removeClass('active');
	    	$('.menu-service').removeClass('active');
	    	$('.menu-trans').removeClass('active');
	    }


	});
</script>




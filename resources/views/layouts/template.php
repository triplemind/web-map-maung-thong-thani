<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Dashboard">
	<meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<title>ระบบช่วยเหลือการเดินทางในเมืองทอง</title>

	<meta property="og:url"           content="http://127.0.0.1:8000/" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="ระบบช่วยเหลือการเดินทางในเมืองทอง" />
	<meta property="og:description"   content="ระบบช่วยเหลือการเดินทางในเมืองทอง เป็นเว็บไซต์ฐานข้อมูล Search Engine ที่ช่วยในการค้นหา จุดแสดงสินค้า จุดอำนวยความสะดวก จุดจอดรถโดยสาร ซึ่งจะสามารถค้นหาได้แค่เฉพาะภายในเมืองทองธานีเท่านั้น" />
	<meta property="og:image"         content="http://127.0.0.1:8000/themes/img/icons8-map-editing-64.png" />

	<!-- Favicons -->
	<link href="/themes/img/favicon.png" rel="icon">
	<link href="/themes/img/apple-touch-icon.png" rel="apple-touch-icon">

	<!-- Bootstrap core CSS -->
	<link href="/themes/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!--external css-->
	<link href="/themes/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="/themes/css/zabuto_calendar.css">
	<link rel="stylesheet" type="text/css" href="/themes/lib/gritter/css/jquery.gritter.css" />
	<!-- Custom styles for this template -->
	<link href="/themes/css/style.css" rel="stylesheet">
	<link href="/themes/css/style-responsive.css" rel="stylesheet">
	<script src="/themes/lib/chart-master/Chart.js"></script>

	<!-- JQuery DataTable Css -->
    <link href="/themes/lib/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <link href="/themes/css/style-responsive.css" rel="stylesheet">

    <link href="/css/map.css" rel="stylesheet">


	<!-- Sweetalert Css -->
    <link href="/themes/lib/sweetalert/sweetalert.css" rel="stylesheet" />


    <!-- bootstrap-material-datetimepicker Css -->
    <link href="/themes/lib/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />

    

	

    <!-- Module CSS -->
    <?php if (file_exists(base_path().'/public/assets/css/'.helperGetModule().'/'.helperGetAction().'.css')): ?>
        <link rel="stylesheet" href="<?php echo '/assets/css/'.helperGetModule().'/'.helperGetAction().'.css?t='.time() ?>">
    <?php endif ?>

    <!-- js placed at the end of the document so the pages load faster -->
	<script src="/themes/lib/jquery/jquery.min.js"></script>

	<script src="/themes/lib/bootstrap/js/bootstrap.min.js"></script>
	<script class="include" type="text/javascript" src="/themes/lib/jquery.dcjqaccordion.2.7.js"></script>
	<script src="/themes/lib/jquery.scrollTo.min.js"></script>
	<script src="/themes/lib/jquery.nicescroll.js" type="text/javascript"></script>
	<script src="/themes/lib/jquery.sparkline.js"></script>
	<!--common script for all pages-->
	<script src="/themes/lib/common-scripts.js"></script>
	<script type="text/javascript" src="/themes/lib/gritter/js/jquery.gritter.js"></script>
	<script type="text/javascript" src="/themes/lib/gritter-conf.js"></script>
	<!--script for this page-->
	<script src="/themes/lib/sparkline-chart.js"></script>
	<script src="/themes/lib/zabuto_calendar.js"></script>

	<script src="/js/site.js"></script>
	<script src="/js/rate.js"></script>
	<script src="/js/map.js"></script>
	<script src="/js/autocomplete.js"></script>
	<script src="/js/star.js"></script>

	<!-- Jquery Sweetalert Js -->
    <script src="/themes/lib/sweetalert/sweetalert.min.js"></script>

    <!-- Bootbox Plugin Js -->
    <script src="/themes/lib/bootstrap-bootbox/bootbox.min.js"></script>

    <!-- datetimepicker Js -->
    <script src="/themes/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>


	
    <!-- <script type="text/javascript" src="http://api.nostramap.com/nostraapi/v2.0?key=Gsc626WMz6k82Oo4SUAdOcqLm6LwVQ(t5LGXoVqzkTb7eBUOtBDHWswHIhjz0bDP3k84OvZeVMl0ug85JRZ8mg0=====2"> </script>  -->

    <div id="fb-root"></div>
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v6.0&appId=225183438519949&autoLogAppEvents=1"></script>


	<!-- Jquery DataTable Plugin Js -->
    <script src="/themes/lib/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/themes/lib/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>

    
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    
    

    <!-- Module js -->
    <?php if (file_exists(base_path().'/public/assets/js/'.helperGetModule().'/'.helperGetAction().'.js')): ?>
        <script src="<?php echo '/assets/js/'.helperGetModule().'/'.helperGetAction().'.js?t='.time() ?>"></script>

    <?php endif ?>
</head>

<body>
	<section id="container">
		<!--header start-->
			<?php echo view('layouts.header');?>
		<!--header end-->

		<!--sidebar start-->
			<?php echo view('layouts.sitebar');?>
		<!--sidebar end-->
	<!-- **********************************************************************************************************************************************************
	MAIN CONTENT
	*********************************************************************************************************************************************************** -->
		<!--main content start-->
			<section id="main-content">
				<?php echo view($page, $data);?>
			</section>
		<!--main content end-->
		<!--footer start-->
			<footer class="site-footer">
				<div class="text-center">
					<p>
					&copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
					</p>
					<div class="credits">
					<!--
					You are NOT allowed to delete the credit link to TemplateMag with free version.
					You can delete the credit link only if you bought the pro version.
					Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/dashio-bootstrap-admin-template/
					Licensing information: https://templatemag.com/license/
					-->
					Created with Dashio template by <a href="https://templatemag.com/">TemplateMag</a>
					</div>
					<a href="index.html#" class="go-top">
						<i class="fa fa-angle-up"></i>
					</a>
				</div>
			</footer>
		<!--footer end-->
	</section>


</body>

</html>

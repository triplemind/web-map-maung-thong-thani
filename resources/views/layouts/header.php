<!--header start-->
<header class="header black-bg">
	<div class="sidebar-toggle-box">
		<div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
	</div>
	<!--logo start-->
	<a href="<?php echo \URL::route('dash.index.get');?>" class="logo-head"><b>ระบบช่วยเหลือการเดินทางใน<span>เมืองทอง</span></b></a>
	<!--logo end-->

	<div class="nav notify-row" id="top_menu">
	<!--  notification start -->

	<!--  notification end -->
	</div>

	<div class="top-menu">
		<?php if(\Session::get('current_user')): ?>
			<ul class="nav pull-right top-menu" style="margin-top: 14px;">
				<li><a class="btn-logout" >ออกจากระบบ</a></li>
			</ul>
		<?php else: ?>

			<ul class="nav pull-right top-menu" style="margin-top: 14px;">
				<li><a class="btn-login" href="<?php echo \URL::route('auth.login.get');?>">ลงชื่อเข้าใช้</a></li>
			</ul>
			
			<ul class="nav pull-right top-menu" style="margin-top: 14px;">
				<li><a class="btn-register" href="<?php echo \URL::route('auth.register.get');?>">ลงทะเบียน</a></li>
			</ul>
		<?php endif ?>


	</div>
</header>

<!-- Data -->
<div id="logout-url"  data-url="<?php echo \URL::route('auth.logout.get');?>"></div>
<section class="wrapper site-min-height">
    <h3><i class="fa fa-angle-right"></i> จัดการผู้ใช้งาน</h3>
    <div class="row mt">
        <div class="col-lg-12">
           <div class="page-head-line">

                <button class="btn btn-info pull-right btn-new-user" style="margin-top: -25px;">
                    <i class="fa fa-plus-circle"></i>
                    <span>New User</span>
                </button>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div style="margin-top: 10px;padding: 20px; ">
                        <table class="table table-striped table-hover js-basic-example dataTable table-responsive">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Image</th>
                                    <th>E-mail</th>
                                    <th>User Type</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($users as $key => $user):?>
                                <tr>
                                    <td><?php echo $key+1; ?></td>
                                    <td><?php echo $user->first_name.' '.$user->last_name; ?></td>
                                    <td><?php echo $user->username; ?></td>
                                    <td>
                                        <?php 
                                        if(!empty($user->user_img)){
                                            $image_show = str_replace("/public","",$user->user_img); 
                                        }
                                        ?>
                                        <img src="<?php echo (empty($user->user_img)) ? url("")."/themes/img/not_found.png" : url("").$image_show; ?>" width="100">
                                    </td>
                                    <td><?php echo $user->email; ?></td>
                                    <td><?php echo $user->user_type; ?></td>
                                    <td>
                                        <div class="row text-center">
                                            <button class=" btn btn-warning waves-effect btn-edit-user" data-user_id="<?php echo $user->user_id; ?>">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                <span>Edit</span>
                                            </button>
                                            <button class="btn btn-danger waves-effect btn-delete-user" data-username="<?php echo $user->username; ?>" data-user_id="<?php echo $user->user_id; ?>">
                                                    <i class="fa fa-trash-o"></i>
                                                <span>Delete</span>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                                
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('user.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('user.add.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('user.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('user.delete.post');?>"></div>
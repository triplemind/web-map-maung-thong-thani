
<section class="wrapper site-min-height">
	<h3><i class="fa fa-angle-right"></i> เพิ่มข้อมูลสถานที่</h3>
	<!-- row -->
	<div class="row mt">
		<div class="col-md-12">
		<div class="content-panel panel-body">
			<form role="form-horizontal" method="post">

				<input type="hidden" class="form-control" name="place_id" placeholder="" value="<?php echo $place->place_id; ?>" />


				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">ชื่อสถานที่ : </label>
					<div class="col-sm-8"  style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="text" name="place_name" value="<?php echo $place->place_name ?>">
					</div>
					<div class="col-sm-2 tt"  style="margin-bottom: 8px;margin-top: 2px;">
						<a class="btn btn-primary btn-local">ค้นหา</a>
					</div>
				</div>

				<div class="form-group other-txt">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">Latitude : </label>
					<div class="col-sm-4" style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="text" name="place_lat" value="<?php echo $place->place_lat ?>">
					</div>

					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">Longitude : </label>
					<div class="col-sm-4" style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="text" name="place_long" value="<?php echo $place->place_long ?>">
					</div>
				</div>



				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">รายละเอียดสถานที่ : </label>
					<div class="col-sm-10"  style="margin-bottom: 8px;margin-top: 2px;">
						<textarea  class="form-control" rows="5" name="place_description" value="<?php echo $place->place_description ?>"></textarea>
					</div>
				</div>

	  			<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">วันที่เปิดทำการ : </label>
					<div class="col-sm-4" style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control"  type="text" name="date_variable" value="<?php echo $place->date_variable ?>">
					</div>

					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">เวลาเปิดทำการ : </label>
					<div class="col-sm-4" style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="text" name="time_open" value="<?php echo $place->time_open ?>">
					</div>
				</div>


				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">สถานะ : </label>
					<div class="col-md-4" style="margin-bottom: 8px;margin-top: 2px;">
						<select  class="form-control" name="place_status">
							<option disabled="disabled">+กรุณาเลือกสถานะ+</option>
							<option value="Active" <?php echo ($place->place_status == "Active") ? "selected" : ""; ?>>Active</option>
							<option value="Inactive" <?php echo ($place->place_status == "Active") ? "selected" : ""; ?>>Inactive</option>
						</select>
					</div>


					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">ประเภทสถานที่ : </label>
					<div class="col-md-4" style="margin-bottom: 8px;margin-top: 2px;">
						<select  class="form-control" name="type_place">
							<option disabled="disabled">+กรุณาเลือกประเภทสถานที่+</option>
							<option value="ห้องน้ำ" <?php echo ($place->type_place == "ห้องน้ำ") ? "selected" : ""; ?>>ห้องน้ำ</option>
							<option value="ธนาคาร" <?php echo ($place->type_place == "ธนาคาร") ? "selected" : ""; ?>>ธนาคาร</option>
							<option value="จุดจอดรถ" <?php echo ($place->type_place == "จุดจอดรถ") ? "selected" : ""; ?>>จุดจอดรถ</option>
							<option value="ที่ทำการไปรษณีย์" <?php echo ($place->type_place == "ที่ทำการไปรษณีย์") ? "selected" : ""; ?>>ที่ทำการไปรษณีย์</option>
							<!-- <option value="สถานที่ส่งของ" <?php //echo ($place->type_place == "สถานที่ส่งของ") ? "selected" : ""; ?>>สถานที่ส่งของ</option> -->
						</select>
					</div>
				</div>

				<div class="form-group other-txt">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">ซอย : </label>
					<div class="col-sm-4" style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="text" name="place_soi" value="<?php echo $place->place_soi ?>">
					</div>

					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">บูธ : </label>
					<div class="col-sm-4" style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="text" name="place_booth" value="<?php echo $place->place_booth ?>">
					</div>
				</div>

				
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">เบอร์โทรศัพท์ : </label>
					<div class="col-sm-4" style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="text" name="place_tel" value="<?php echo $place->place_tel ?>">
					</div>

					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">โลโก้หรือรูปตัวอย่าง : </label>
					<div class="col-sm-4"  style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="file" name="place_logoimage" value="">
						<!-- <div style="font-size: 12px;color: red;font-weight: bold;text-align: right;">*รูปภาพขนาด 1080x800 เฉพาะไฟล์ .png, .jpg  จำนวน ไม่เกิน 25 รูป</div> -->
					</div>
				</div>


				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">รูปเพิ่มเติม : </label>
					<div class="col-sm-10"  style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="file" multiple name="place_allimage" value="">
						<!-- <div style="font-size: 12px;color: red;font-weight: bold;text-align: right;">*รูปภาพขนาด 1080x800 เฉพาะไฟล์ .png, .jpg  จำนวน ไม่เกิน 25 รูป</div> -->
					</div>
				</div>

			</form>

			<div style="margin-top: 8px;"></div>

		    <div class="col-md-12" style="text-align: right;margin-bottom: 10px;">
		    	<button class="btn btn-primary btn-small btn-update-data">SAVE</button>
		    	<button class="btn btn-defalt btn-small btn-cancel-data" onclick="window.location.href = '/place'; ">CANCEL</button>
		    </div>

		</div>
		<!-- /content-panel -->
		</div>
		<!-- /col-md-12 -->
	</div>
</section>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='update-url' data-url="<?php echo \URL::route('service.edit.post');?>"></div>
    


<script>
	$(function(){

		$('.btn-local').on('click', function(){
			geocode();
		});


		$('.datetimepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose : true,
        });


		$('.btn-update-data').on('click', function(){
			msg_waiting();
			var id 					= $('input[name=place_id]').val();
			var place_name 			= $('input[name=place_name]').val();
			var place_description 	= $('textarea[name=place_description]').val();
			var date_variable 		= $('input[name=date_variable]').val();
			var time_open 			= $('input[name=time_open]').val();
			var place_tel 			= $('input[name=place_tel]').val();
			var place_status 		= $('select[name=place_status]').val();
			var type_place 			= $('select[name=type_place]').val();
			var place_soi 			= $('input[name=place_soi]').val();
			var place_booth 		= $('input[name=place_booth]').val();
			var place_lat 			= $('input[name=place_lat]').val();
			var place_long 			= $('input[name=place_long]').val();

			var update_url    	= $('#update-url').data('url');
			var data        = new FormData();

			if (typeof($('input[name=place_logoimage]')[0]) !== 'undefined') {

	            jQuery.each(jQuery('input[name=place_logoimage]')[0].files, function(i, file) {
	                data.append('place_logoimage', file);
	            });
	        }

	        if (typeof($('input[name=place_allimage]')[0]) !== 'undefined') {

	            jQuery.each(jQuery('input[name=place_allimage]')[0].files, function(i, file) {
	                data.append('place_allimage[]', file);
	            });
	        }


	        console.log(update_url);
	        console.log(place_long);

			data.append('id', id);
			data.append('place_name', place_name);
			data.append('place_description', place_description);
			data.append('date_variable', date_variable);
			data.append('time_open', time_open);
			data.append('place_tel', place_tel);
			data.append('place_status', place_status);
			data.append('type_place', type_place);
			data.append('place_soi', place_soi);
			data.append('place_booth', place_booth);
			data.append('place_lat', place_lat);
			data.append('place_long', place_long);

			$.ajax({
	        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
	        type: 'post',
	        url: update_url,
	        data: data,
	        contentType: false,
	        processData:false,
	        cache: false,
	        success: function(result) {
	            if(result.status == 'success'){
	                // window.location.reload();
	                window.location.href = "/service";
	            } // End if check s tatus success.

	            if(result.status == 'error'){
	                
	            }
	        },
	        error : function(error) {
	            console.log(error);
	        }
	    });


		});

	});


	function geocode(){

		var location = $('input[name=place_name]').val();

		$.ajax({
	        url: "https://maps.googleapis.com/maps/api/geocode/json?address="+location+"&key=AIzaSyAqwZuazvlcmJxuUpC9q9P1mjiR_xjEMfE", 
	        type: "GET",   
	        responseType:'application/json',
	        cache: false,
	        success: function(response){  

	        	if(response.status == "OK"){

	                console.log(response); 
	                console.log(response.status); 
	                console.log(response.results[0].geometry.location.lat); 
	                console.log(response.results[0].geometry.location.lng); 
	        	
	        		$('input[name=place_lat]').val(response.results[0].geometry.location.lat);
	        		$('input[name=place_long]').val(response.results[0].geometry.location.lng);   
	        	}

	        	if(response.status == "ZERO_RESULTS"){
	        		msg_error_custom("ไม่พบชื่อที่ค้นหา", "กรุณาพิมพ์คำค้นหาใหม่อีกครั้ง");
	        	}
	         
	        },
	        error : function(error) {
	            console.log(error);
	            msg_error_custom(error.statusText, error.responseJSON.error_message);
	        }             
	    });    
	 
	}

		

</script>
    

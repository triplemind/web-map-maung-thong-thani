<section class="wrapper site-min-height">
    <h3><i class="fa fa-angle-right"></i> จัดการข้อมูลระบบขนส่งสาธารณะ</h3>
    <div class="row mt">
        <div class="col-lg-12">
           <div class="page-head-line">

                <button class="btn btn-info pull-right btn-new-transport" style="margin-top: -25px;">
                    <i class="fa fa-plus-circle"></i>
                    <span>เพิ่มข้อมูลใหม่</span>
                </button>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div style="margin-top: 10px;padding: 20px; ">
                        <table class="table table-striped table-hover js-basic-example dataTable table-responsive">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>โลโก้หรือรูปตัวอย่าง</th>
                                    <th>ชื่อ</th>
                                    <th>วันที่เปิดทำการ</th>
                                    <th>เวลาเปิดทำการ</th>
                                    <th>สถานะ</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php if($transports->count() !== 0): ?>
                                <?php foreach($transports as $key => $transport):?>
                                <tr>
                                    <td><?php echo $key+1; ?></td>
                                    <td><?php echo $transport->transport_name; ?></td>
                                    <td>
                                        <?php 
                                        if(!empty($transport->transport_logoimage)){
                                            $image_show = str_replace("/public","",$transport->transport_logoimage); 
                                        }
                                        ?>
                                        <img src="<?php echo (empty($transport->transport_logoimage)) ? url("")."/themes/img/not_found.png" : url("").$image_show; ?>" width="100">
                                    </td>
                                    <td><?php echo $transport->time_open; ?></td>
                                    <td><?php echo $transport->date_variable; ?></td>
                                    <td><?php echo $transport->transport_status; ?></td>
                                    <td>
                                        <div class="row text-center">
                                            <button class=" btn btn-warning waves-effect btn-edit-transport" data-transport_id="<?php echo $transport->transport_id; ?>">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                <span>Edit</span>
                                            </button>
                                            <button class="btn btn-danger waves-effect btn-delete-transport" data-place_name="<?php echo $transport->transport_name; ?>" data-transport_id="<?php echo $transport->transport_id; ?>">
                                                    <i class="fa fa-trash-o"></i>
                                                <span>Delete</span>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
	                            <?php endforeach ?>
	                        <?php endif ?>
                                
                            </tbody>
                        </table>
                        <?php //echo $transports->render(); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('transport.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('transport.add.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('transport.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('transport.delete.post');?>"></div>

<script>
	$(function(){

        $('.js-basic-example').dataTable();

		$('.btn-new-transport').on('click', function(){
			window.location.href = "/transport/add";
		});

		$('.js-basic-example').on('click', '.btn-edit-transport', function(){

			window.location.href = "/transport/edit/"+$(this).data('transport_id');
		});


		// DELETE USER
        $('.js-basic-example').on('click', '.btn-delete-transport', function(){
            var place_id = $(this).data('transport_id');
            swal({
                  title: "Are you sure?",
                  text: "Do you want delete this transport => "+$(this).data('place_name')+" ?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: true,
                  closeOnCancel: true
                },
                  function(isConfirm) {
                  if (isConfirm) {
                       msg_waiting();
                    postRemove(transport_id);

                  }
             });
        });

	});


function postRemove(transport_id)
{
    var delete_url      = $('#delete-url').data('url');

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: delete_url,
        data: {
             'transport_id' : transport_id,
        },
        success: function(result) {
            if(result.status == 'success'){
                window.location.reload();
            } // End if check s tatus success.
        },
        error : function(error) {
            msg_error_custom('Error!', error);
        }
    });
}
	
</script>
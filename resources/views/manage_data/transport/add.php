
<section class="wrapper site-min-height">
	<h3><i class="fa fa-angle-right"></i> เพิ่มข้อมูลสถานที่</h3>
	<!-- row -->
	<div class="row mt">
		<div class="col-md-12">
		<div class="content-panel panel-body">
			<form role="form-horizontal" method="post">

				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">ชื่อ : </label>
					<div class="col-sm-8"  style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="text" name="transport_name" value="">
					</div>
					<div class="col-sm-2 tt"  style="margin-bottom: 8px;margin-top: 2px;">
						<a class="btn btn-primary btn-local">ค้นหา</a>
					</div>
				</div>

				<div class="form-group other-txt">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">Latitude : </label>
					<div class="col-sm-4" style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="text" name="transport_lat" value="0.0000">
					</div>

					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">Longitude : </label>
					<div class="col-sm-4" style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="text" name="transport_long" value="0.0000">
					</div>
				</div>


				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">รายละเอียด : </label>
					<div class="col-sm-10"  style="margin-bottom: 8px;margin-top: 2px;">
						<textarea  class="form-control" rows="5" name="transport_description" value=""></textarea>
					</div>
				</div>

	  			<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">วันที่เปิดทำการ : </label>
					<div class="col-sm-4" style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="text" name="date_variable" value="">
					</div>

					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">เวลาเปิดทำการ : </label>
					<div class="col-sm-4" style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="text" name="time_open" value="">
					</div>
				</div>


				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">สถานะ : </label>
					<div class="col-md-4" style="margin-bottom: 8px;margin-top: 2px;">
						<select  class="form-control" name="transport_status">
							<option disabled="disabled">+กรุณาเลือกสถานะ+</option>
							<option value="Active">Active</option>
							<option value="Inactive">Inactive</option>
						</select>
					</div>


					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">ประเภท : </label>
					<div class="col-md-4" style="margin-bottom: 8px;margin-top: 2px;">
						<select  class="form-control" name="type_transport">
							<option disabled="disabled">+กรุณาเลือกประเภท+</option>
							<option value="ป้ายรถเมล์">ป้ายรถเมล์</option>
							<option value="วินมอเตอร์ไซค์">วินมอเตอร์ไซค์</option>
							<option value="คิวรถตู้">คิวรถตู้</option>
							<option value="shuttle">รถ Shuttle bus</option>
						</select>
					</div>
				</div>

				<div class="form-group other-txt">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">ราคาต่ำที่สุด : </label>
					<div class="col-sm-4" style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="text" name="transport_lowprice" value="">
					</div>

					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">ราคาสูงที่สุด : </label>
					<div class="col-sm-4" style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="text" name="transport_hightprice" value="">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">โลโก้หรือรูปตัวอย่าง : </label>
					<div class="col-sm-10"  style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="file" name="transport_logoimage" value="">
						<!-- <div style="font-size: 12px;color: red;font-weight: bold;text-align: right;">*รูปภาพขนาด 1080x800 เฉพาะไฟล์ .png, .jpg  จำนวน ไม่เกิน 25 รูป</div> -->
					</div>
				</div>


				<!-- <div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label" style="margin-bottom: 8px;margin-top: 2px;">รูปเพิ่มเติม : </label>
					<div class="col-sm-10"  style="margin-bottom: 8px;margin-top: 2px;">
						<input  class="form-control" type="file" multiple name="place_allimage" value="">
						<div style="font-size: 12px;color: red;font-weight: bold;text-align: right;">*รูปภาพขนาด 1080x800 เฉพาะไฟล์ .png, .jpg  จำนวน ไม่เกิน 25 รูป</div>
					</div>
				</div> -->

			</form>

			<div style="margin-top: 8px;"></div>

		    <div class="col-md-12" style="text-align: right;margin-bottom: 10px;">
		    	<button class="btn btn-primary btn-small btn-save-data">SAVE</button>
		    	<button class="btn btn-defalt btn-small btn-cancel-data" onclick="window.location.href = '/transport'; ">CANCEL</button>
		    </div>

		</div>
		<!-- /content-panel -->
		</div>
		<!-- /col-md-12 -->
	</div>
</section>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('transport.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('transport.add.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('transport.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('transport.delete.post');?>"></div>
    


<script>
	$(function(){

		$('.btn-local').on('click', function(){
			geocode();
		});


		// $('.datetimepicker').datepicker({
  //           format: 'yyyy-mm-dd',
  //           autoclose : true,
  //       });


		$('.btn-save-data').on('click', function(){
			msg_waiting();
			var transport_name 			= $('input[name=transport_name]').val();
			var transport_description 	= $('textarea[name=place_description]').val();
			var date_variable 			= $('input[name=date_variable]').val();
			var time_open 				= $('input[name=time_open]').val();
			var transport_status 		= $('select[name=transport_status]').val();
			var type_transport 			= $('select[name=type_transport]').val();
			var transport_lowprice 		= $('input[name=transport_lowprice]').val();
			var transport_hightprice 	= $('input[name=transport_hightprice]').val();
			var transport_lat 			= $('input[name=transport_lat]').val();
			var transport_long 			= $('input[name=transport_long]').val();

			var add_url    	= $('#add-url').data('url');
			var data        = new FormData();

			if (typeof($('input[name=transport_logoimage]')[0]) !== 'undefined') {

	            jQuery.each(jQuery('input[name=transport_logoimage]')[0].files, function(i, file) {
	                data.append('transport_logoimage', file);
	            });
	        }

	        // if (typeof($('input[name=place_allimage]')[0]) !== 'undefined') {

	        //     jQuery.each(jQuery('input[name=place_allimage]')[0].files, function(i, file) {
	        //         data.append('place_allimage[]', file);
	        //     });
	        // }


			data.append('transport_name', transport_name);
			data.append('transport_description', transport_description);
			data.append('date_variable', date_variable);
			data.append('time_open', time_open);
			data.append('transport_status', transport_status);
			data.append('type_transport', type_transport);
			data.append('transport_lowprice', transport_lowprice);
			data.append('transport_hightprice', transport_hightprice);
			data.append('transport_lat', transport_lat);
			data.append('transport_long', transport_long);

			$.ajax({
	        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
	        type: 'post',
	        url: add_url,
	        data: data,
	        contentType: false,
	        processData:false,
	        cache: false,
	        success: function(result) {
	            if(result.status == 'success'){
	                // window.location.reload();
	                window.location.href = "/transport";
	            } // End if check s tatus success.

	            if(result.status == 'error'){
	                
	            }
	        },
	        error : function(error) {
	            console.log(error);
	        }
	    });


		});

	});


	function geocode(){

		var location = $('input[name=transport_name]').val();

		$.ajax({
	        url: "https://maps.googleapis.com/maps/api/geocode/json?address="+location+"&key=AIzaSyAqwZuazvlcmJxuUpC9q9P1mjiR_xjEMfE", 
	        type: "GET",   
	        responseType:'application/json',
	        cache: false,
	        success: function(response){  

	        	if(response.status == "OK"){

	                console.log(response); 
	                console.log(response.status); 
	                console.log(response.results[0].geometry.location.lat); 
	                console.log(response.results[0].geometry.location.lng); 
	        	
	        		$('input[name=transport_lat]').val(response.results[0].geometry.location.lat);
	        		$('input[name=transport_long]').val(response.results[0].geometry.location.lng);   
	        	}

	        	if(response.status == "ZERO_RESULTS"){
	        		msg_error_custom("ไม่พบชื่อที่ค้นหา", "กรุณาพิมพ์คำค้นหาใหม่อีกครั้ง");
	        	}
	         
	        },
	        error : function(error) {
	            console.log(error);
	            msg_error_custom(error.statusText, error.responseJSON.error_message);
	        }             
	    });    
	 
	}

		

</script>
    

var locations;
var markers = [];
var map, infoWindow, pos;
var _alldata;
var _posi;




$(window).load(function() {
    $('.search-data').on('click', function(){
        msg_waiting();
        var title = $(this).data('title');
        addFilterToSession(title);
    });

    var ajax_url      = $('#ajax-center-url-dash').data('url');
    var method        = 'getDataToSelected';
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
        },
        success: function(result) {
            if(result.status == "success"){

                _alldata = result.data;

                console.log(_alldata);
                
				autocomplete(document.getElementById("searchonly"), _alldata);
            }
        }
    });

    // $(".btn-search").on('click', function(){
    //     msg_waiting();
    //     var title = $("input[name=search]").val();
    //     addFilterToSession(title);
    // });

    

    $('.del').on('click', function(){
        window.location.reload();
    });

});

function initMap() {

	var directionsDisplay = new google.maps.DirectionsRenderer();
	var directionsService = new google.maps.DirectionsService();

    // สร้าง map
    map = new google.maps.Map(document.getElementById('map'), {
        // mapTypeControl: false,
        center: {lat: 13.9129232, lng: 100.5477901},
        zoom: 16
    });

    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById('right-panel'));
    // สร้าง map

    $('.btn-direction').on('click', function(){

        var ajax_url      = $('#ajax-center-url-dash').data('url');
        var method        = 'getDataToSelected';

        $.ajax({
            headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
            type: 'post',
            url: ajax_url,
            data: {
                'method' : method,
            },
            success: function(result) {
                console.log(result);

                // $('.btn-search-direction').css('display','none');
                $('.btn-search-direction').css('display','unset');
                $('.startp').css('display','unset');

                $('.btn-direction').css('display','none');
                $('.btn-search').css('display','none');
                // $('.del').css('display','none');
                $('input[name=search]').css('display','none');

                if(result.status == "success"){

                    _alldata = result.data;

                    console.log(_alldata);
                    autocomplete(document.getElementById("origin-input"), _alldata);
                    autocomplete(document.getElementById("destination-input"), _alldata);
                }

                // new AutocompleteDirectionsHandler(map);

            }
        });

    });


    $('.btn-search-direction').on('click', function(){ 
        
        var start 		= document.getElementById("origin-input").value;
        var end 		= document.getElementById("destination-input").value;
        var ajax_url    = $('#ajax-center-url-dash').data('url');

        $.ajax({
            headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
            type: 'post',
            url: ajax_url,
            data: {
                'method' : "getDataToMap",
                'start' : start,
                'end' : end,
            },
            success: function(result) {
            	if(result.status == "success"){
            		console.log(result.data);
            		console.log(result.data.length);

            		if(result.data.length <= 1){
            			swal({
	                        title:'ไม่พบสถานที่ ที่คุณค้นหา', 
	                        text: 'กรุณาพิมพ์คำค้นหาใหม่อีกครั้ง', 
	                        type:'error', 
	                        confirmButtonText:  'OK'});
            		}else{
	            		console.log(result.data[1].start);
	            		console.log(result.data[1].end);

	            		var start = result.data[1].start;
    					var end = result.data[1].end;

						directionsService.route({
							origin:start, 
							destination:end,
							travelMode: 'TRANSIT'

						}, function(response, status) {
							if (status === 'OK') {
								directionsDisplay.setDirections(response);
							} else {
								window.alert('Directions request failed due to ' + status);
							}
						});
						document.getElementById('right-panel').style.backgroundColor = "#fff";


            		}

            	}

            }
        });

    });

    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {

        	_posi = position.coords.latitude+","+position.coords.longitude;

        	console.log(_posi);

            pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('ฉันอยู่ที่นี่.');
            infoWindow.open(map);
            map.setCenter(pos);

            console.log(pos);
            var clickHandler = new ClickEventHandler(map, pos);

            var marker = new google.maps.Marker({
                position: map.getCenter(pos),
                    icon: {
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: 10
                    },
                        draggable: true,
                        map: map
                });

            
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }


    $(".btn-search").on('click', function(){
        msg_waiting();
        var title = $("input[name=search]").val();

        var ajax_url      = $('#ajax-center-url-dash').data('url');
		var method        = 'getRouteForSearch';
		$.ajax({
		    headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		    type: 'post',
		    url: ajax_url,
		    data: {
		        'method' : method,
		        'title' : title,
		    },
		    success: function(result) {
		        if(result.status == "success"){
		        	console.log(result.data);
            		console.log(result.data.length);

            		if(result.data.length <= 0){
            			swal({
	                        title:'ไม่พบสถานที่ ที่คุณค้นหา', 
	                        text: 'กรุณาพิมพ์คำค้นหาใหม่อีกครั้ง', 
	                        type:'error', 
	                        confirmButtonText:  'OK'});
            		}else{
            			msg_stop();
            			
	            		console.log(result.data[0].start);
	            		console.log("dfdd: "+_posi);

    					var start = "15.870032000000002,100.99254099999999";
	            		var end = result.data[0].start;

						directionsService.route({
							origin:start, 
							destination:end,
							travelMode: 'DRIVING'

						}, function(response, status) {
							if (status === 'OK') {
								directionsDisplay.setDirections(response);
							} else {
								window.alert('Directions request failed due to ' + status);
							}
						});
						document.getElementById('right-panel').style.backgroundColor = "#fff";
            		}
		        }
		    }
		});
        // addFilterToSession(title);
    });


}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                          'Error: The Geolocation service failed.' :
                          'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}


/**
 * @constructor
 */
 // ฟังก์ชั่นการสร้างเส้นทาง
// function AutocompleteDirectionsHandler(map) {
// 	console.log("sfsf");
// 	this.map = map;
// 	this.originPlaceId = null;
// 	this.destinationPlaceId = null;
// 	this.travelMode = 'TRANSIT';
// 	this.directionsService = new google.maps.DirectionsService;
// 	this.directionsRenderer = new google.maps.DirectionsRenderer;
// 	this.directionsRenderer.setMap(map);
// 	this.directionsRenderer.setPanel(document.getElementById('right-panel'));

//     var originInput = document.getElementById('origin-input');
//     var destinationInput = document.getElementById('destination-input');

//     var originAutocomplete = new google.maps.places.Autocomplete(originInput);
//     originAutocomplete.setFields(['place_id']);

//     var destinationAutocomplete =
//     new google.maps.places.Autocomplete(destinationInput);
//     destinationAutocomplete.setFields(['place_id']);

//     var control = document.getElementById('floating-panel');


//     this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
//     this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');

//     this.map.controls[google.maps.ControlPosition.TOP_CENTER].push(control);

// }
// ฟังก์ชั่นการสร้างเส้นทาง


// AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(
//     autocomplete, mode) {
//     var me = this;
//     autocomplete.bindTo('bounds', this.map);

//     autocomplete.addListener('place_changed', function() {
//         var place = autocomplete.getPlace();

//         if (!place.place_id) {
//             window.alert('Please select an option from the dropdown list.');
//             return;
//         }
//         if (mode === 'ORIG') {
//             me.originPlaceId = place.place_id;
//         } else {
//             me.destinationPlaceId = place.place_id;
//         }
//         me.route();
//     });
// };


// สร้างเส้นทาง
// AutocompleteDirectionsHandler.prototype.route = function() {
// 	// if (!this.originPlaceId || !this.destinationPlaceId) {
// 	//   return;
// 	// }
// 	var me = this;

// 	console.log("dsd");
// 	console.log("cxxxx");

// 	this.directionsService.route(
// 	{

// 		origin: {'placeId': this.originPlaceId},
// 		destination: {'placeId': this.destinationPlaceId},
// 		travelMode: this.travelMode
// 	},
// 	function(response, status) {
// 		if (status === 'OK') {
// 			me.directionsRenderer.setDirections(response);
// 		} else {
// 			window.alert('Directions request failed due to ' + status);
// 		}
// 	});

// 	document.getElementById('right-panel').style.backgroundColor = "#fff";
// };
// สร้างเส้นทาง


/**
* @constructor
*/
var ClickEventHandler = function(map, origin) {
	this.origin = origin;
	this.map = map;
	this.directionsService = new google.maps.DirectionsService;
	this.directionsRenderer = new google.maps.DirectionsRenderer;
	this.directionsRenderer.setMap(map);
	this.placesService = new google.maps.places.PlacesService(map);
	this.infowindow = new google.maps.InfoWindow;
	this.infowindowContent = document.getElementById('infowindow-content');
	this.infowindow.setContent(this.infowindowContent);
	this.directionsRenderer.setPanel(document.getElementById('right-panel'));

	// Listen for clicks on the map.
	this.map.addListener('click', this.handleClick.bind(this));

	var control = document.getElementById('floating-panel');

	this.map.controls[google.maps.ControlPosition.TOP_CENTER].push(control);
};



ClickEventHandler.prototype.handleClick = function(event) {
	// this.directionsRenderer.setDirections(null);
    console.log('You clicked on: ' + event.latLng);
    if (event.placeId) {
        console.log('You clicked on place:' + event.placeId);

        event.stop();
        this.calculateAndDisplayRoute(event.placeId);
    }
};


ClickEventHandler.prototype.calculateAndDisplayRoute = function(placeId) {
	// directionsRenderer.setMap(null);
    var me = this;
    this.directionsService.route({
        origin: this.origin,
        destination: {placeId: placeId},
        travelMode: 'DRIVING'
    }, function(response, status) {
        if (status === 'OK') {
            me.directionsRenderer.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });

    document.getElementById('right-panel').style.backgroundColor = "#fff";
};


function addFilterToSession(title)
{
    var ajax_url      = $('#ajax-center-url-dash').data('url');
    var method        = 'addFilterToSession';

    console.log(title);
    
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: ajax_url,
		data: {
			'method' : method,
			'title' : title,
		},
        success: function(result) {
            if (result.status === 'success'){
                console.log(result.data.length);
                if(result.data.length == 0){
                    // alert("ไม่พบสถานที่ ที่คุณค้นหา กรุณาพิมพ์คำค้นหาใหม่อีกครั้ง"); 
                    swal({
                        title:'ไม่พบสถานที่ ที่คุณค้นหา', 
                        text: 'กรุณาพิมพ์คำค้นหา หรือ เลือกหัวข้อการค้นหา ใหม่อีกครั้ง', 
                        type:'error', 
                        confirmButtonText:  'OK'});
                }else{
                	msg_stop();
                }

                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }
                markers = [];

                console.log(result.datamark);
                locations = result.datamark;
                var infowindow = new google.maps.InfoWindow();

                var marker, i, contentString,a,b,c,d,e,f,g;

                for (i = 0; i < locations.length; i++) {  
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map
                    });
                	console.log("dsdsds : "+locations[i][6]);

	                a = (locations[i][0] == null) ? "  -" : locations[i][0];
	                b = (locations[i][1] == null) ? "  -" : locations[i][1];
	                c = (locations[i][2] == null) ? "  -" : locations[i][2];
	                d = (locations[i][3] == null) ? "  -" : locations[i][3];
	                e = (locations[i][4] == null) ? "  -" : locations[i][4];
	                f = (locations[i][5] == null) ? "  -" : locations[i][5];
	                g = (locations[i][6] == "" || locations[i][6] == null) ? "/themes/img/404.png" : locations[i][6].replace("/public", "");
	                h = (locations[i][8] == null) ? "  -" : locations[i][8];


            		if(locations[i][7] == "ร้านอาหาร" || locations[i][7] == "โรงแรม" || locations[i][7] == "ห้างสรรพสินค้า" || locations[i][7] == "ร้านกาแฟ" || locations[i][7] == "สถานที่ออกกำลังกาย" || locations[i][7] == "สถานบันเทิง" || locations[i][7] == "จุดแสดงสินค้า"){

		                contentString = '<div id="content" style="overflow-y: scroll;max-height: 350px;width:550px;">'+
		                        '<div id="siteNotice" style="text-align: center;">'+
		                        '<img width="65%" src="'+g+'">'+
		                        '</div>'+
		                            '<h1 id="firstHeading" class="firstHeading">'+a+'</h1>'+
		                            '<div id="bodyContent">'+
		                                '<div class="fb-share-button"                                                  data-href="https://muangthong.000webhostapp.com/"  data-layout="button" data-size="small">'+
		                                '<div data-target="_blank" data-action="share-facebook" onclick="AddAction()" data-href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fmuangthong.000webhostapp.com%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore ac-add"><img src="/themes/img/icons8-facebook-480.png" style="width: 6%"/> แชร์ </div>'+
		                                '</div>'+
		                                '<p>คะแนน : <span class="fa fa-star checked"></span>'+
		                                            '<span class="fa fa-star checked"></span>'+
		                                            '<span class="fa fa-star checked"></span>'+
		                                            '<span class="fa fa-star"></span>'+
		                                            '<span class="fa fa-star"></span></p>'+
		                                '<p>วันที่ :'+e+'</p>'+
		                                '<p>เวลา :'+f+'</p>'+
		                                '<p><b>'+a+'</b>'+d+'</p>'+
		                            '</div>'+

		                            '<div class="text-right" style="padding: 10px;">'+
		                                '<button class="btn btn-info btn-get-review" data-place_id="'+h+'" type="submit" onclick="getModalReview('+h+')">รีวิว</button>'+
		                            '</div>'+
		                    '</div>';
            		}else{
		                contentString = '<div id="content" style="overflow-y: scroll;max-height: 350px;width:550px;">'+
		                        '<div id="siteNotice" style="text-align: center;">'+
		                        '<img width="65%" src="'+g+'">'+
		                        '</div>'+
		                            '<h1 id="firstHeading" class="firstHeading">'+a+'</h1>'+
		                            '<div id="bodyContent">'+
		                                '<p>วันที่ :'+e+'</p>'+
		                                '<p>เวลา :'+f+'</p>'+
		                                '<p><b>'+a+'</b>'+d+'</p>'+
		                            '</div>'+
		                    '</div>';
		            }

                    google.maps.event.addListener(marker, 'click', (function(marker, i, contentString, infowindow) {
	                    return function() {
	                        infowindow.setContent(contentString);
	                        infowindow.open(map, marker);
	                    }
                    })(marker, i, contentString, infowindow));

                    markers.push(marker);
                }

                console.log("ss:"+markers);

                $('.del').on('click', function(){
                    DeleteMarkers(markers);
                    // window.location.reload();
                });

            }

        },
    });

}


function AddAction() {
    
	var action_sh   = $('.ac-add').data('action');
	var href        = $('.ac-add').data('href');
	var target      = $('.ac-add').data('target');
	var add_url_dash = $('#add-url-dash').data('url');

	console.log(add_url_dash);
	console.log(href);
	console.log(action_sh);
	console.log(target);

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        type: "POST",
        url: add_url_dash,
        data: {action_sh : action_sh},
        success: function(Response) {
            if(Response.status == "success"){

                window.open(href, target, "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
            }
        }
    });
   
}



function getModalReview(id) {

    var ajax_url      = $('#ajax-center-url-dash').data('url');
    var method        = 'getModalReview';

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: "POST",
        url: ajax_url,
        data: {'id' : id, 'method' : method,},
        success: function(Response) {
            if(Response.status == "success"){
                $('#modai-review').modal('show');

                $('.getreview').empty();

                for (var i = 0; i < Response.data.length; i++) {
                    
                    $('.modal-title').html(Response.data[i].place.place_name);

                    console.log(Response.data);
                    console.log(Response.data[i].place.place_name);
                    console.log(Response.data[i].review_text);
                    console.log(Response.data[i].review_date);
                    console.log(Response.data[i].point);
                    console.log(Response.data[i].user.username);
                    console.log(Response.data[i].user.user_img);

                    var imgg = (Response.data[i].user.user_img == "" || Response.data[i].user.user_img == null) ? "/themes/img/404.png" : Response.data[i].user.user_img.replace("/public", "");

                    var text = '<div style="padding: 20px;border-bottom: 1px solid;">'+
                                '<p><img src="'+imgg+'" style="width: 10%;">'+Response.data[i].user.username+'</p>'+
                                '<div>'+Response.data[i].review_text+'</div>'+
                                '<div class="text-right" style="padding-top5px;">'+
                                    '<div class="starrr hearts-existing" data-rating="'+Response.data[i].point+'"style="font-size: 25px;"></div>'+
                                        'You gave a rating of <span class="count-existing">'+Response.data[i].point+'</span> heart(s)'+
                                '</div>'+
                            '</div>';

                    $('.getreview').append(text);
                }

                $('.btn-save-review').on('click', function(){
                    var chk = $(this).data('chk_user');
                    console.log(chk);

                    if(chk == 1){
                        postAddRate(id);
                    }else{
                        swal({
                            title:'ERROR', 
                            text: 'กรุณาเข้าสู่ระบบก่อนแสดงความคิดเห็น', 
                            type:'error', 
                            confirmButtonText:  'OK'},
                            function(){
                              window.location.href = "/auth/login";
                        });
                    }
                });

            }
        }
    });
}


function postAddRate(id) {
    
    var newReview = $('#new-review').val();
    var ratingsField = $('#rate').val();

    var add_rate_dash =$('#add-rate-dash').data('url');

    console.log(ratingsField);
    console.log(newReview);

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: "POST",
        url: add_rate_dash,
        data: {'id' : id, 'review_text' : newReview, 'point' : ratingsField,},
        success: function(Response) {
            if(Response.status == "success"){
                // msg_stop();
                swal({
                    title:'SUCCESS', 
                    text: 'ขอบคุณสำหรับการให้คะแนนค่ะ', 
                    type:'success', 
                    confirmButtonText:  'OK'},
                    function(){
                        $('#modai-review').modal('hide');
                });

            }
        }
    });

}


function DeleteMarkers(markers) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers = [];
}



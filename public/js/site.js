$(function(){

    $('.btn-logout').on('click', function(){
            swal({
            title: "Are you sure?",
            text: "Do you want to sign out ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function(isConfirm) {
            if (isConfirm) {
                msg_waiting();
                logout_url = $('#logout-url').data('url');
                window.location = logout_url;

            }
        });
    });


    

    // Change language.
    // $('.change-lang').on('click', function(){
    //     msg_waiting();
    //     var ajax_url    = $('#change-lang-url').data('url');
    //     var lang        = $(this).data('value');
    //     $.ajax({
    //         headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
    //         type: 'post',
    //         url: ajax_url,
    //         data: {
    //             'lang' : lang,
    //         },
    //         success: function(result) {
    //             window.location.reload();
    //         },
    //     });
    // });

});

function msg_waiting() {
    swal({title: '', html: true, imageUrl: '/themes/img/loading.gif', type:'', showConfirmButton: false});
}
function msg_process() {
    swal({title:'', text: 'Processing', imageUrl: '/themes/img/loading.gif', type:'info', showConfirmButton: false});
}
function msg_progress_upload() {
    swal({html: true, title:'Please wait...', text: '', imageUrl: '/themes/img/loading.gif', type:'info', showConfirmButton: false});
}
function msg_success() {
    swal({title:'', text: 'Submit Successful', type:'success', confirmButtonText:  'OK', timer: 10000});
}
function msg_success_custom(title, msg) {
    swal({title:title, text: msg, type:'success', timer: 5000});
}
function msg_success_without_button() {
    swal({title:'', text: 'Submit Successful', type:'success', showConfirmButton: false, timer: 10000});
}
function msg_unsuccess(message) {
    swal({title:'', text: message, type:'error', confirmButtonText:  'OK', timer: 10000});
}
function msg_error_custom(title, message) {
    swal({title: title, html: true, text:message, type:'error', confirmButtonText:  'OK'});
}
function msg_info(message) {
    swal({title:'', text: message, confirmButtonText:  'OK'});
}
function msg_stop() {
    swal.close();
}
function update_progress(txt) {
    $('.text-muted').text(txt);
}

function showValidate(validation)
{
    if(validation.status == 422){
        jQuery.each(validation.responseJSON, function(k ,value){
            $('.text-error-' +k).html('* ' + value[0]);
        });
        // console.log(typeof(validation.responseText));
       if(typeof(validation.responseText) == 'string'){
            jQuery.each(JSON.parse(validation.responseText), function(k ,value){
                $('.text-error-' +k).html('* ' + value[0]);
            });
       }
    }
}

function showValidateObject(validation)
{
   Object.keys(validation).map(function(objectKey, index) {
    var value = validation[objectKey];
        $('.text-error-' +objectKey).html('* ' + value);
    });
}

function showOldInput(data)
{
    jQuery.each(data, function(k, val){
        if(k != 'user_type') $('input[name='+k+']').val(val);
        if(k == 'user_type') $('select[name='+k+']').val(val);
    });
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


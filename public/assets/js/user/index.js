$(function(){

	// ADD NEW USER
	$('.btn-new-user').on('click', function(){
		msg_waiting();
		getFormAddUser();
	});

	// CALL Data Table
	$('.js-basic-example').DataTable();

	// EDIT USER
	$('.js-basic-example').on('click', '.btn-edit-user', function(){
		msg_waiting();
		getFormEditUser($(this).data('user_id'));
	});

	// DELETE USER
	$('.js-basic-example').on('click', '.btn-delete-user', function(){
		var user_id = $(this).data('user_id');
		swal({
	          title: "Are you sure?",
	          text: "Do you want delete username => "+$(this).data('username')+" ?",
	          type: "warning",
	          showCancelButton: true,
	          confirmButtonClass: "btn-danger",
	          confirmButtonText: "Yes",
	          cancelButtonText: "No",
	          closeOnConfirm: true,
	          closeOnCancel: true
	        },
	          function(isConfirm) {
	          if (isConfirm) {
	               msg_waiting();
				postRemove(user_id);

	          }
	     });
	});



});


function getFormAddUser(){

	var method 		= 'getFormAddUser';
	var ajax_url	= $('#ajax-center-url').data('url');
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: ajax_url,
		data: {
		  	'method' : method,
		},
		success: function(result) {
			if(result.status == 'success'){
				msg_stop();
				showForm(result.form, 'add');
			} // End if check s tatus success.
		}
	});

}

function getFormEditUser(user_id)
{
	var method 		= 'getFormEditUser';
	var ajax_url	= $('#ajax-center-url').data('url');
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: ajax_url,
		data: {
		  	'method' : method,
		  	'user_id' : user_id,
		},
		success: function(result) {
			if(result.status == 'success'){
				msg_stop();
				showForm(result.form, 'edit');
			} // End if check s tatus success.
		}
	});

}


function showForm(form, action, validation = '', data = '')
{
	var box = bootbox.dialog({
		className: 'bootbox-show-detail-magazine',
		message : form,
		backdrop: true,
		closeButton: false,
		size: 'large',
		buttons : {
			danger: {
			    label: 'close',
			    className: 'btn-default',
			    callback: function() {
			        box.modal("hide");
			    }
			},
			success: {
			    label: 'Save',
			    className: 'btn-primary',
			    callback: function() {
			     if(action == 'add')	postAdd(form, action);
			     if(action == 'edit')	postEdit(form, action);

			    }
			}
		},
	});
	 box.on('shown.bs.modal', function(){
			// $.AdminBSB.input.activate();
			// $.AdminBSB.select.activate();
	});
}



function postAdd(form, action)
{
	var username 		= $('input[name=username]').val();
	var email 			= $('input[name=email]').val();
	var password 		= $('input[name=password]').val();
	var user_type 		= $('select[name=user_type]').val();
	var first_name 		= $('input[name=first_name]').val();
	var last_name 		= $('input[name=last_name]').val();
	var add_url			= $('#add-url').data('url');
	var data  			= new FormData();

	if (typeof($('input[name=image]')[0]) !== 'undefined') {

        jQuery.each(jQuery('input[name=image]')[0].files, function(i, file) {
            data.append('image', file);
        });
    }


	data.append('username', username);
	data.append('email',email);
	data.append('password',password);
	data.append('user_type',user_type);
	data.append('first_name',first_name);
	data.append('last_name',last_name);


	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: add_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				msg_waiting();
				window.location.reload();
			} // End if check s tatus success.

			if(result.status == 'error'){
				
			}
		},
		error : function(error) {
			showForm(form, action, error, data);
		}
	});

}


function postEdit(form, action)
{
	var user_id 		= $('input[name=user_id]').val();
	var username 		= $('input[name=username]').val();
	var email 			= $('input[name=email]').val();
	var password 		= $('input[name=password]').val();
	var user_type 		= $('select[name=user_type]').val();
	var first_name 		= $('input[name=first_name]').val();
	var last_name 		= $('input[name=last_name]').val();
	var edit_url		= $('#edit-url').data('url');
	var data  			= new FormData();

	if (typeof($('input[name=image]')[0]) !== 'undefined') {

        jQuery.each(jQuery('input[name=image]')[0].files, function(i, file) {
            data.append('image', file);
        });
    }

	data.append('user_id', user_id);
	data.append('username', username);
	data.append('email',email);
	data.append('password',password);
	data.append('user_type',user_type);
	data.append('first_name',first_name);
	data.append('last_name',last_name);

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: edit_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				msg_waiting();
				window.location.reload();
			} // End if check s tatus success.

			if(result.status == 'error'){
				
			}
		},
		error : function(error) {
			console.log(error);
			showForm(form, action, error, data);
		}
	});


}


function postRemove(user_id)
{
	var delete_url		= $('#delete-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: delete_url,
		data: {
		  	 'user_id' : user_id,
		},
		success: function(result) {
			if(result.status == 'success'){
				msg_waiting();
				window.location.reload();
			} // End if check s tatus success.
		},
		error : function(error) {
			msg_error_custom('Error!', error);
		}
	});
}